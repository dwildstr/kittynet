#!/usr/bin/env python3
import sys,datetime,copy,numpy,configparser,logging,glob


config=configparser.ConfigParser()
config.read(glob.glob("/home/dwildstr/panopticon.d/*.cfg"))

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s')
log = logging.getLogger(__name__)
log.setLevel(logging.WARNING)
if "Logging" in config.sections() and "level" in config["Logging"]:
    if config["Logging"]["level"][0].lower()=='d':
        log.setLevel(logging.DEBUG)
    elif config["Logging"]["level"][0].lower()=='i':
        log.setLevel(logging.INFO)
    elif config["Logging"]["level"][0].lower()=='w':
        log.setLevel(logging.WARNING)
    elif config["Logging"]["level"][0].lower()=='e':
        log.setLevel(logging.ERROR)
    elif config["Logging"]["level"][0].lower()=='c':
        log.setLevel(logging.CRITICAL)
    else:
        log.setLevel(logging.WARNING)
        log.warning("Unrecognized log level; defaulting to WARNING.")

from kittynet import common,plaintext,util


import flask

### Build bluetooth monitoring service and all cats

allcats=common.Pipeline()
catLEDColors={}
catWebConfig={}
try:
    from kittynet import bluetooth
    ### Cat bluetooth monitoring service (All messages piped through allcats)
    btMonitor=bluetooth.LEMonitor(frequency=2)

    ### Add configuration-file-specified cats to the pipeline
    for section in config.sections():
        if section.startswith("Cat-"):
            try:
                deviceName=config.get(section,"name",fallback=section[4:])
                rssiAdjust=config.getint(section,"rssiadjust",fallback=0)
                if config.get(section,"device",fallback=None)=="miband2":
                    btDevice=bluetooth.MiBand2(addr=config[section]["address"],
                                               name=deviceName,
                                               rssi_adjust=rssiAdjust)
                else:
                    ### Fallback is to make device generic
                    btDevice=bluetooth.LEDevice(addr=config[section]["address"],
                                                name=deviceName,
                                                rssi_adjust=rssiAdjust)
                btMonitor.add_device(btDevice)
                allcats.add_server(btDevice)
                if "color" in config[section]:
                    import colour
                    catLEDColors[deviceName]=colour.Color(config[section]["color"]).rgb
                catWebConfig[deviceName]={}
                if "webcolor" in config[section]:
                    catWebConfig[deviceName]["backgroundColor"]=config[section]["webcolor"]
                if "webborder" in config[section]:
                    catWebConfig[deviceName]["borderColor"]=config[section]["webborder"]
                if "webpointstyle" in config[section]:
                    catWebConfig[deviceName]["pointStyle"]=config[section]["webpointstyle"]
                log.info("Bluetooth device {} registered".format(section))
            except KeyError as e:
                log.error("Malformed configuration section {}, missing key {}".format(section,str(e)))
            except ImportError as e:
                log.error("Colour package not installed: {}".format(e))
                sys.exit()
            except ValueError as e:
                log.error("Bad color in configuration: {}".format(e))
                sys.exit()
    btMonitor.start()
    log.info("Bluetooth monitor successfully initialized")
except ImportError as e:
    log.error("Bluetooth modules not installed: {}".format(e))
    sys.exit()

### Build configuration-file-specified battery alerts, and add their
### message systems to the alerts dict.

alerts={}

for section in config.sections():
    if section.startswith("Battery-"):
        try:
            messageFormatter=util.Transformer(transform_function=lambda m:{"subject":"Kittynet battery alert for {}".format(m["name"]),"msg":"Device {} reports battery level {}".format(m["name"],m["battery"])})
            common.chain(allcats,
                         util.BatteryAlert(config.getfloat(section,"alert"),reset=config.getfloat(section,"reset")),
                         messageFormatter)
            alerts[section]=messageFormatter
            log.info("Battery alert {} registered".format(section))
        except KeyError as e:
            log.error("Malformed configuration section {}, missing key {}".format(section,str(e)))
            sys.exit()
        except ValueError as e:
            log.error("Expected a numeric value in section {}: {}".format(section,e))
            sys.exit()


### Hardware weight sensor configuration: the HX711 reports raw
### weights, which have a long pathway of message handlers to pass
### through before they get reported anywhere user-visible.
    
if "HX711" in config.sections():
    try:
        from kittynet import pigpiodev
        loadSensor=pigpiodev.HX711(data_pin=config.getint("HX711","data"),
                                   clock_pin=config.getint("HX711","clock"),
                                   channel='A',
                                   gain=128,
                                   ratio=config.getfloat("HX711","ratio"),
                                   name="HX711")

        # I've had trouble with HX711s randomly getting wedged, so this
        # watchdog should reset it if, for some reason, it stops responding.
        watchdog=util.Watchdog(time=60)
        watchdog.watch(loadSensor)
        loadSensor.start()
        log.info("HX711 initialized")
    except ImportError as e:
        log.error("Failed to setup HX711: {}".format(e))
        sys.exit()
    except KeyError as e:
        log.error("Malformed configuration section {}, missing key {}".format(section,str(e)))
        sys.exit()
    except ValueError as e:
        log.error("Expected a numeric value in section {}: {}".format(section,e))
        sys.exit()
else:
    ### This seems like an unlikely use case, but if a user (for
    ### whatever reason) chooses not to configure a load sensor, we'll
    ### just add a dummy load sensor that never sends messages, so
    ### that the rest of the message chain can be built around it.
    log.warning("No HX711 section in configuration; no weight sensor initiated.")
    loadSensor=common.Server()


### There are three potential filters to suppress reports from the
### load sensor: any number of manual switches to turn the load sensor
### on or off, a report from a CatGenie-monitoring Arduino to
### temporarily suspend weight monitoring, or a report from a "fake
### Arduino" indicating a CatGenie wash cycle.

loadHead=loadSensor

### Hardware switch filters
for section in config.sections():
    if section.startswith("Switch-"):
        try:
            from kittynet import gpiodev
            switch=gpiodev.ToggleFilter(pin=config[section]["pin"])
            loadHead.add_client(switch)
            loadHead=switch
            log.info("{} hardware switch instantiated".format(section))
        except ImportError as e:
            log.error("Failed to setup hardware switch: {}".format(e))
            sys.exit()
        except KeyError as e:
            log.error("Malformed configuration section {}, missing key {}".format(section,str(e)))
            sys.exit()
        except ValueError as e:
            log.error("Expected an integer pin number in section {}: {}".format(section,e))
            sys.exit()

if "Catgenie" in config.sections():
    try:
        from kittynet import serialdev
        arduinoMon=serialdev.SerialReader(device=config["Catgenie"]["device"],name="CatGenie")
        genieInterrupt=util.Deactivator(triggername="CatGenie",sleeptime=config.getfloat("Catgenie","delay"))
        common.chain(arduinoMon,
                     util.Filter(filter_function = lambda m:m["data"]<121),
                     genieInterrupt)
        arduinoMon.start()
        log.info("Arduino Catgenie monitor initialized")
    except ImportError as e:
        log.error("Failed to setup Arduino monitor: {}".format(e))
        sys.exit()
    except KeyError as e:
        log.error("Malformed configuration section {}, missing key {}".format(section,str(e)))
        sys.exit()
    except ValueError as e:
        log.error("Expected an integer pin number in section {}: {}".format(section,e))
        sys.exit()
else:
    ### Implement genieInterrupt as a dummy pipeline
    log.warning("No Catgenie section in config; Catgenie monitoring disabled")
    genieInterrupt=common.Pipeline(threaded=False)

if "Fake-Catgenie" in config.sections():
    try:
        fcg_time_min=config.getfloat("Fake-Catgenie","time_min")
        fcg_time_max=config.getfloat("Fake-Catgenie","time_max")
        fcg_weight_min=config.getfloat("Fake-Catgenie","weight_min")
        fcg_weight_max=config.getfloat("Fake-Catgenie","weight_max")
        fakeArduinoInterrupt=util.Deactivator(triggername="Fake-CatGenie",sleeptime=config.getfloat("Fake-Catgenie","delay"))
    except KeyError as e:
        log.error("Malformed configuration section {}, missing key {}".format(section,str(e)))
        sys.exit()
    except ValueError as e:
        log.error("Expected a numeric value in section {}: {}".format(section,e))
        sys.exit()
    def fake_arduino(message):
        if message["wtype"]!="high":
            return message
        if (message["length"].total_seconds()<fcg_time_min) or (message["length"].total_seconds()>fcg_time_max):
            return message
        if (message["weight"]<fcg_weight_min) or (message["weight"]>fcg_weight_max):
            return message
        # If we got this far, this is actually a wash cycle; replace
        # the message with a wash-cycle alert.
        return {"time":message["time"],
                "name":"Fake-CatGenie",
                "wtype":"clean",
                "data":"No serial",}
    fakeArduinoMon=util.Transformer(transform_function=fake_arduino)
    common.chain(fakeArduinoMon,
                 util.Filter(filter_function = lambda m:m["wtype"]=="clean"),
                 fakeArduinoInterrupt)
    log.info("Weight-based Catgenie monitor initialized")
else:
    # Implement the fake Arduino monitor/interrupt as a dummy pipeline
    log.warning("No Fake-Catgenie section in config; Catgenie weigt-based monitoring disabled")
    fakeArduinoInterrupt=common.Pipeline(threaded=False)
    fakeArduinoMon=common.Pipeline()

### Post-interrupt load sensor data feeds into the weight processor

if "Weight" in config.sections():
    try:
        weightInterpreter=util.WeightProcessor(weight_threshold=config.getfloat("Weight","threshold"),tare_variability=config.getfloat("Weight","tare_variability"),transition_time=config.getfloat("Weight","transition_time"))
        common.chain(loadHead,
                     genieInterrupt,
                     fakeArduinoInterrupt,
                     weightInterpreter)
        log.info("Weight processor initialized")
    except KeyError as e:
        log.error("Malformed configuration section {}, missing key {}".format(section,str(e)))
        sys.exit()
    except ValueError as e:
        log.error("Expected a numeric value in section {}: {}".format(section,e))
        sys.exit()
else:
    log.warning("No Weight section in config. WEIGHT PROCESSING DISABLED. This is probably not intended.")
    weightInterpreter=common.Pipeline(threaded=False)

### Weight-event packager and cat-guessing routines

packager=util.RSSIWeightPackager()
allcats.add_client(packager)

### An ad-hoc routine to guess the cat associated with a given
### packaged weight event; at present it uses RSSI strength
### exclusively, but weight guesses will be added as a secondary feature.

def appendCatGuess(message):
    newmessage=copy.deepcopy(message)
    bestGuess="Unknown"
    bestRSSI=-55
    for cat,rssi in message["rssis"].items():
        if rssi>bestRSSI:
            bestGuess=cat
            bestRSSI=rssi
    newmessage["catguess"]=bestGuess
    return newmessage

catGuesser=util.Transformer(transform_function=appendCatGuess)

### The cat guesser is the final element after the interpreter. Note that the fake Arduino monitor takes input from the weight interpreter, to guess at catgenie clean cycles.

common.chain(weightInterpreter,
             fakeArduinoMon,
             packager,
             catGuesser)


### We add an alert type for weight alerts specifically

messageFormatter=util.Transformer(transform_function=lambda m:{"subject":"Kittynet visit alert for {}".format(m["catguess"]),"msg":"{} visit by {} with weight {} and time {}".format(config["Panopticon"]["name"],m["catguess"],m["weight"],m["length"])})
catGuesser.add_client(messageFormatter)
alerts["Weight"]=messageFormatter

### The alerts dict is set up, so we now set up our alert endpoints:
### GMail and Twilio

if "GMail" in config.sections():
    try:
        from kittynet import google
        gmailer=google.GMailAlerter(
            manager=google.ServiceManager(
                config["Google"]["credentials"],
                service='gmail',api='v1',
                scopes=google.SCOPES_GMAIL,
                token=config["GMail"]["token"]),
            recipient=config["GMail"]["recipient"],
            sender="Kittynet alerts <{}>".format(config["GMail"]["address"]))
        for alertname in alerts:
            if alertname in config["GMail"]["alerts"]:
                alerts[alertname].add_client(gmailer)
        log.info("GMail alert system initialized")
    except ImportError as e:
        log.error("Failed to setup Google services: {}".format(e))
        sys.exit()
    except KeyError as e:
        log.error("Malformed configuration section {}, missing key {}".format(section,str(e)))
        sys.exit()
else:
    log.warning("No GMail section in config; GMail functionality disabled")
    
arduinoPipeline=util.Filter(filter_function = lambda m:"data" in m)
if "Catgenie" in config.sections():
    arduinoMon.add_client(arduinoPipeline)
if "Fake-Catgenie" in config.sections():
    fakeArduinoMon.add_client(arduinoPipeline)
    
### Google sheets integration

if "GSheets" in config.sections():
    try:
        from kittynet import google
        sheetsManager=google.ServiceManager( 
            config["Google"]["credentials"],
            service='sheets',api='v4',
            scopes=google.SCOPES_SHEETS,
            token=config["GSheets"]["token"])
        log.info("Sheets manager initialized")

        ### Sheets reporting for Catgenie events
        
        common.chain(arduinoPipeline,
                     google.SheetsLogger(
                         manager=sheetsManager,
                         sheet_id=config["GSheets"]["sheetID"],
                         pagename="Genie events",
                         rowspec=["{time}",config["Panopticon"]["name"],"{data}"]))
        log.info("Sheets Genie-logger initialized")

        ### Google sheets reports for cat-usage events

        if config.getboolean("GSheets","bycat",fallback=True):
            catGuesser.add_client(google.SheetsLogger(
                manager=sheetsManager,
                sheet_id=config["GSheets"]["sheetID"],
                pagename="{catguess} events",
                rowspec=["{time}","{weight}",config["Panopticon"]["name"],"{length}","{rssis}","{leavings}"]))
        if config.getboolean("GSheets","bybox",fallback=True):
            catGuesser.add_client(google.SheetsLogger(
                manager=sheetsManager,
                sheet_id=config["GSheets"]["sheetID"],
                pagename=config["Panopticon"]["name"] + " events",
                rowspec=["{time}","{weight}","{catguess}","{length}","{rssis}","{leavings}"]))
        if config.getboolean("GSheets","onesheet",fallback=True):
            catGuesser.add_client(google.SheetsLogger(
                manager=sheetsManager,
                sheet_id=config["GSheets"]["sheetID"],
                pagename="All weight events",
                rowspec=["{time}","{weight}",config["Panopticon"]["name"],"{catguess}","{length}","{rssis}","{leavings}"]))
        log.info("GSheets weight-logger initialized")
    except ImportError as e:
        log.error("Failed to setup Google services: {}".format(e))
        sys.exit()
    except KeyError as e:
        log.error("Malformed configuration section {}, missing key {}".format(section,str(e)))
        sys.exit()
else:
    log.warning("No GSheets section in config; sheets functionality disabled")

if "Twilio" in config.sections():
    try:
        from kittynet import twilio
        twilio=twilio.Twilio(sid=config["Twilio"]["sid"],
                             token=config["Twilio"]["token"],
                             recipient=config["Twilio"]["recipient"],
                             sender=config["Twilio"]["number"])
        for alertname in alerts:
            if alertname in config["Twilio"]["alerts"]:
                alerts[alertname].add_client(twilio)
        log.info("Twilio alert system initialized")
    except ImportError as e:
        log.error("Failed to setup Twilio client: {}".format(e))
        sys.exit()
    except KeyError as e:
        log.error("Malformed configuration section {}, missing key {}".format(section,str(e)))
        sys.exit()
else:
    log.warning("No Twilio section in config; Twilio functionality disabled")

if "notify.run" in config.sections():
    try:
        from kittynet import notify
        notify=notify.Notify(endpoint=config.get("notify.run","endpoint",fallback=None),
                             api_server=config.get("notify.run","endpoint",fallback=None))
        for alertname in alerts:
            if alertname in config["notify.run"]["alerts"]:
                alerts[alertname].add_client(notify)
        log.info("notify.run alert system initialized")
    except ImportError as e:
        log.error("Failed to setup notify.run client: {}".format(e))
        sys.exit()
    except KeyError as e:
        log.error("Malformed configuration section {}, missing key {}".format(section,str(e)))
        sys.exit()
else:
    log.warning("No notify.run section in config; notify.run functionality disabled")

### LED controls; all LEDs are now connected to appropriate endpoints

for section in config.sections():
    if section.startswith("LED-"):
        try:
            from kittynet import gpiodev
            if "red" in config[section]:
                ### Full color LEDs
                if config[section]["display"]=="bluetooth":
                    allcats.add_client(gpiodev.RGBLEDBTSource(red=config[section]["red"],green=config[section]["green"],blue=config[section]["blue"],sources=catLEDColors))
                elif config[section]["display"]=="weight":
                    loadSensor.add_client(gpiodev.RGBLEDCycle(red=config[section]["red"],green=config[section]["green"],blue=config[section]["blue"],direction=False))
                else:
                    log.error("Unknown display type {} for full-color LED in section {}".format(config[section]["display"],section))
                    sys.exit()
            else:
                ### Monochrome LEDs
                if config[section]["display"]=="bluetooth":
                    allcats.add_client(gpiodev.LEDToggle(pin=config[section]["pin"]))
                elif config[section]["display"]=="weight":
                    loadSensor.add_client(gpiodev.PWMLEDPulse(pin=config[section]["pin"]))
                else:
                    log.error("Unknown display type {} for single-color LED in section {}".format(config[section]["display"],section))
                    sys.exit()
            log.info("{} successfully set up".format(section))
        except ImportError as e:
            log.error("Failed to import GPIO module: {}".format(e))
            sys.exit()
        except KeyError as e:
            log.error("Malformed configuration section {}, missing key {}".format(section,str(e)))
            sys.exit()
                

### Flask integration; passes all Bluetooth messages, all
### weight-events, and an aggregated version of the perpetual weight
### reports to the Flask endpoint. The aggregation is to keep a spam
### of messages from choking the endpoint, or overfilling the buffer.

if "Flask" in config.sections():
    try:
        import flask as sysflask
        from kittynet import flask as kittyflask
        webapp=sysflask.Flask(__name__)
        @webapp.route('/')
        def www_index():
            return sysflask.render_template('index.html')

        flaskClient=kittyflask.FlaskPath(webapp,config["Flask"]["path"],buffer_length=config.getint("Flask","bufferlength"))
        flaskClient.add_header_message({"name":"webconfig",
                                        "time":datetime.datetime.now(),
                                        "data":catWebConfig})
        common.chain(
            loadSensor,
            util.Aggregator(interval=config.getint("Flask","aggregation"),numeric_aggregator=numpy.median),
            flaskClient)
        weightInterpreter.add_client(flaskClient)
        allcats.add_client(flaskClient)
        arduinoPipeline.add_client(flaskClient)
        log.info("Flask endpoint built")
    except ImportError as e:
        log.error("Failed to import Flask module: {}".format(e))
        sys.exit()
    except KeyError as e:
        log.error("Malformed configuration section {}, missing key {}".format(section,str(e)))
        sys.exit()
    except ValueError as e:
        log.error("Expected a numeric value in section {}: {}".format(section,e))
        sys.exit()
else:
    log.warning("No Flask section in config; Flask functionality disabled")
        
### Everything's ready to go, so now we either start Flask, or start a
### do-nothing loop.

if "Flask" in config.sections():
    webapp.config['TEMPLATES_AUTO_RELOAD'] = True
    webapp.run(host=config["Flask"]["host"],port=config["Flask"]["port"],threaded=True)
else:
    try:
        while True:
            pass
    except KeyboardInterrupt:
        pass


### This is all cleanup; if it fails, it fails.

try:
    btMonitor.stop()
except:
    pass
try:
    loadSensor.stop()
except:
    pass
try:
    arduinoMon.stop()
except:
    pass
