"""Support for HX711 load cell devices connected over GPIO.

Logically this should be subsumed into gpiodev, and may be some day,
but since it uses a separate hx711 package it seemed best to separate
it out for those who don't need HX711 support. Also, gpiodev uses
gpiozero, but gpiozero's onboard hx711 support is a bit
unpredictable.

"""

import hx711
from .common import Server
from numbers import Number
import time,datetime,threading,copy,numpy

class HX711(Server):
    """HX711 load sensor monitor, which streams weight reports.

    This server will broadcast scaled data from an HX711 load
    sensor. The messages it broadcasts will have the weight in the
    ``"weight"`` field. Note that the weight will be scaled to
    whatever ratio is in use, but will not, in general, be
    tared. Passing these reports to a
    :class:`kittynet.util.WeightProcessor` pipeline is usually the
    best way to make use of them.

    Parameters
    ----------
    data_pin : int
        GPIO pin number (using BCM numbering) of the data channel,
        typically labeled on the HX711 chip as "DOUT" or "DT".
    clock_pin : int
        GPIO pin number (using BCM numbering) of the clock channel,
        typically labeled on the HX711 chip as "SCK".
    channel : str
        Channel to use on the HX711 chip. Should be "A" or "B", each
        corresponding to a pair of pins on the chip. See the HX711
        specifications for technical details.
    gain : int
        Gain to request from the HX711 chip. Supported gains for
        channel A are 128 and 64; gain for channel B must be 32. See
        the HX711 specifications for technical details.
    ratio : float or int
        Conversion factor for the load cell to the units of your
        choice (grams, pounds, kilograms, etc.). This factor may need
        to be experimentally determined.
    sleep : float or int
        Number of seconds to sleep between reports. Measurements
        themselves take a significant amount of time due to the
        required clock pulsing, so even sleep time zero (the default)
        produces a manageable level of messages.
    measurements : int
        Number of measurements to take per report. The reported value
        is the median of the entire measurement set. A measurement
        value of at least 3 is advisable if your HX711 is prone to
        stray wild figures, since the median of a set of three values,
        at most one of which is wildly inaccurate, is likely to give a
        trustworthy result.

    Attributes
    ----------
    ratio : float or int
        Conversion factor for the load cell to reported units
    sleep : float or int
        Number of seconds to sleep between reports.
    measurements : int
        Number of measurements to take per report.

    """
    
    def __init__(self,data_pin=5,clock_pin=6,channel='A',gain=64,ratio=1,sleep=0,measurements=5,name="HX711"):
        self._hx711=hx711.HX711(dout_pin=data_pin,pd_sck_pin=clock_pin,channel=channel,gain=gain)
        self._hx711.reset()
        self.name=name
        self.ratio=ratio
        self.sleep=sleep
        self.measurements=measurements
        self._thread=threading.Thread()
        self.running=False
        super().__init__()

    def start(self):
        """Start the scanning thread which produces reported weights regularly.
        
        If the thread is already active, this command does nothing.

        """

        if self.running:
            return
        self._thread=threading.Thread(target=self._scanloop)
        self.running=True
        self._thread.start()

    def stop(self):
        """Terminates the running thread after completing the current scan.

        If the thread is not running, this method does nothing.

        """
        self.running=False
      
    def _scanloop(self):
        while self.running:
            raw_data=self._hx711.get_raw_data(times=self.measurements)
            self.broadcast_data({
                "name":self.name,
                "time":datetime.datetime.now(),
                "weight":numpy.median(raw_data)/self.ratio})
            time.sleep(self.sleep)
