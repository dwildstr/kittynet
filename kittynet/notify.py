"""notify.run integration form KittyNet

This package has a simple client which uses the notify.run API to send
messages as push notifications to cellphones or other devices set up
to receive them.

Read the notify.run documentation for information on how to configure channels. By default this uses both the preconfigured systemwide channel and the standard API server, but both of those are reconfigurable.
"""

import notify_run

from .common import Client

class Notify(Client):
    """A Kittynet client which formats received messages as strings
    (attaching a hyperlink if desired) and then sends them to the
    provided notify.run endpoint so that subscribers to that
    endpoint's associated channel receive push notifications.

    Parameters
    ----------
    endpoint : str, optional
        The endpoint to be used. If not provided (or set to None),
        then the system endpoint in ~/.config/notify-run (probably set
        by running notify-register) is used.
    api_server : str, optional
        The API server to be used, if not the default. Unless you're
        running your own notify.run server (which there's not really a
        good reason to do in most circumstances), there's no reason to
        use this.
    text : str
        Text of the notifications sent. This string will be formatted using
        the method :meth:`str.format` and can include placeholders
        expanded into message values.
    url : str, optional
        URL of the notifications sent. This string will be formatted
        using the method :meth:`str.format` and can include
        placeholders expanded into message values. If missing, None,
        or evaluating to an empty string after formatting, then the
        notification will not be a hyperlink.

    """

    def __init__(self,endpoint=None,api_server=None,text="{msg}",url=None):
        self._notifier=notify_run.Notify(api_server=api_server,endpoint=endpoint)
        self.text=text
        self.url=url
        super().__init__()

    def handle_data(self,message):
        """The message being handled is sent to the notify.run API

        Parameters
        ----------
        message : dict
            Message received from a server, to be sent.
        """
        if self.url is None:
            url=None
        else:
            url=self.url.format(**message)
            if url=="":
                url=None
        self._notifier.send(message=self.text.format(**message),action=url)
