"""Support for devices using pigpiod

Logically this should be subsumed into gpiodev, and may be some day,
but since it uses a separate package (pigpio) it seemed best to
separate it out. I originally started using this because pigpio's more
precise timing makes for a less error-ridden access to the HX711 chip, which is sensitive to long pulses.

"""

import pigpio
from .common import Server
from numbers import Number
import time,datetime,threading,copy,numpy


HX711_CH_A_GAIN_64  = 0 # Channel A gain 64
HX711_CH_A_GAIN_128 = 1 # Channel A gain 128
HX711_CH_B_GAIN_32  = 2 # Channel B gain 32

HX711_DATA_CLKS = 24
HX711_X_128_CLK = 25
HX711_X_32_CLK  = 26
HX711_X_64_CLK  = 27


class HX711Workhorse(object):
   """Shamelessly stolen from the HX711 example for the pigpio library.

   """
   def __init__(self, pi, DATA, CLOCK, mode=HX711_CH_A_GAIN_128, callback=None,PULSE_LENGTH=1,SETTLE_READINGS=5):
      """
      Instantiate with the Pi, the data GPIO, and the clock GPIO.

      Optionally the channel and gain may be specified with the
       mode parameter as follows.

      CH_A_GAIN_64  - Channel A gain 64
      CH_A_GAIN_128 - Channel A gain 128
      CH_B_GAIN_32  - Channel B gain 32

      Optionally a callback to be called for each new reading may be
      specified.  The callback receives three parameters, the count,
      the mode, and the reading.  The count is incremented for each
      new reading.
      """
      self.pi = pi
      self.DATA = DATA
      self.CLOCK = CLOCK
      self.callback = callback
      self._settle_readings = SETTLE_READINGS
      self._pulse_length = PULSE_LENGTH
      self._timeout = ((HX711_X_64_CLK + 3) * 2 * PULSE_LENGTH)
      
      self._paused = True
      self._data_level = 0
      self._clocks = 0

      self._mode = HX711_CH_A_GAIN_128
      self._value = 0

      self._rmode = HX711_CH_A_GAIN_128
      self._rval = 0
      self._count = 0

      self._sent = 0
      self._data_tick = pi.get_current_tick()
      self._previous_edge_long = False
      self._in_wave = False

      self._skip_readings = self._settle_readings

      pi.write(CLOCK, 1) # Reset the sensor.

      pi.set_mode(DATA, pigpio.INPUT)

      pi.wave_add_generic(
         [pigpio.pulse(1<<CLOCK, 0, self._pulse_length),
          pigpio.pulse(0, 1<<CLOCK, self._pulse_length)])

      self._wid = pi.wave_create()

      self._cb1 = pi.callback(DATA, pigpio.EITHER_EDGE, self._callback)
      self._cb2 = pi.callback(CLOCK, pigpio.FALLING_EDGE, self._callback)

      self.set_mode(mode)

   def get_reading(self):
      """
      Returns the current count, mode, and reading.

      The count is incremented for each new reading.
      """
      return self._count, self._rmode, self._rval

   def set_callback(self, callback):
      """
      Sets the callback to be called for every new reading.
      The callback receives three parameters, the count,
      the mode, and the reading.  The count is incremented
      for each new reading.

      The callback can be cancelled by passing None.
      """
      self.callback = callback

   def set_mode(self, mode):
      """
      Sets the mode.

      CH_A_GAIN_64  - Channel A gain 64
      CH_A_GAIN_128 - Channel A gain 128
      CH_B_GAIN_32  - Channel B gain 32
      """
      self._mode = mode

      if mode == HX711_CH_A_GAIN_128:
         self._pulses = HX711_X_128_CLK
      elif mode == HX711_CH_B_GAIN_32:
         self._pulses = HX711_X_32_CLK
      elif mode == HX711_CH_A_GAIN_64:
         self._pulses = HX711_X_64_CLK
      else:
         raise ValueError

      self.pause()

   def pause(self):
      """
      Pauses readings.
      """
      self._skip_readings = self._settle_readings
      self._paused = True
      self.pi.write(self.CLOCK, 1)
      time.sleep(0.002)
      self._clocks = HX711_DATA_CLKS + 1

   def start(self):
      """
      Starts readings.
      """
      self._wave_sent = False
      self.pi.write(self.CLOCK, 0)
      self._clocks = HX711_DATA_CLKS + 1
      self._value = 0
      self._paused = False
      self._skip_readings = self._settle_readings

   def cancel(self):
      """
      Cancels the sensor and release resources.
      """
      self.pause()

      if self._cb1 is not None:
         self._cb1.cancel()
         self._cb1 = None

      if self._cb2 is not None:
         self._cb2.cancel()
         self._cb2 = None

      if self._wid is not None:
         self.pi.wave_delete(self._wid)
         self._wid = None

   def _callback(self, gpio, level, tick):

      if gpio == self.CLOCK:

         if level == 0:

            self._clocks += 1

            if self._clocks <= HX711_DATA_CLKS:

               self._value = (self._value << 1) + self._data_level

               if self._clocks == HX711_DATA_CLKS:

                  self._in_wave = False

                  if self._value & 0x800000: # unsigned to signed
                     self._value |= ~0xffffff

                  if not self._paused:

                     if self._skip_readings <= 0:

                        self._count = self._sent
                        self._rmode = self._mode
                        self._rval = self._value

                        if self.callback is not None:
                           self.callback(self._count, self._rmode, self._rval)

                     else:
                        self._skip_readings -= 1

      else:
         current_edge_long = None
         self._data_level = level         
         
         if not self._paused:

            if self._data_tick is not None:

               current_edge_long = pigpio.tickDiff(
                  self._data_tick, tick) > self._timeout

            if current_edge_long and not self._previous_edge_long:

               if not self._in_wave:

                  self._in_wave = True

                  self.pi.wave_chain(
                     [255, 0, self._wid, 255, 1, self._pulses, 0])

                  self._clocks = 0
                  self._value = 0
                  self._sent += 1

         self._data_tick = tick
         self._previous_edge_long = current_edge_long


class HX711(Server):
    """HX711 load sensor monitor, which streams weight reports.

    This server will broadcast scaled data from an HX711 load
    sensor. The messages it broadcasts will have the weight in the
    ``"weight"`` field. Note that the weight will be scaled to
    whatever ratio is in use, but will not, in general, be
    tared. Passing these reports to a
    :class:`kittynet.util.WeightProcessor` pipeline is usually the
    best way to make use of them.

    A lot of the code used to generate the bitbanging part of this is
    taken from the pigpio examples page, which has a specific HX711 handler.

    Parameters
    ----------
    data_pin : int
        GPIO pin number (using BCM numbering) of the data channel,
        typically labeled on the HX711 chip as "DOUT" or "DT".
    clock_pin : int
        GPIO pin number (using BCM numbering) of the clock channel,
        typically labeled on the HX711 chip as "SCK".
    channel : str
        Channel to use on the HX711 chip. Should be "A" or "B", each
        corresponding to a pair of pins on the chip. See the HX711
        specifications for technical details.
    gain : int
        Gain to request from the HX711 chip. Supported gains for
        channel A are 128 and 64; gain for channel B must be 32. See
        the HX711 specifications for technical details.
    ratio : float or int
        Conversion factor for the load cell to the units of your
        choice (grams, pounds, kilograms, etc.). This factor may need
        to be experimentally determined.

    Attributes
    ----------
    ratio : float or int
        Conversion factor for the load cell to reported units

    """
    
    def __init__(self,data_pin,clock_pin,channel='A',gain=64,ratio=1,name="HX711",pulse_width=15):
        self.name=name
        self._data_pin=data_pin
        self._clock_pin=clock_pin
        self._pulse_width=pulse_width
        if (channel=="A") and (gain==64):
           self._mode=HX711_CH_A_GAIN_64
        elif (channel=="A") and (gain==128):
           self._mode=HX711_CH_A_GAIN_128
        elif (channel=="B") and (gain==32):
           self._mode=HX711_CH_B_GAIN_32
        else:
           raise ValueError("Channel and gain must be A64, A128, or B32 on an HX711.")
        self.ratio=ratio
        self._pigpio=pigpio.pi()
        if not self._pigpio.connected:
           raise Exception("Pigpio is not connected; pigpiod may not be running.")
        super().__init__()

    def __del__(self):
        self._pigpio.stop()

    def _rcv_weight(self,count,mode,reading):
        self.broadcast_data({"name":self.name,
                             "time":datetime.datetime.now(),
                             "weight":reading/self.ratio})

    def start(self):
        """Start reporting weights.

        """
        self._sensor=HX711Workhorse(pi=self._pigpio,
                                    DATA=self._data_pin,
                                    CLOCK=self._clock_pin,
                                    mode=self._mode,
                                    PULSE_LENGTH=self._pulse_width)
        self._sensor.start()
        self._sensor.set_callback(self._rcv_weight)
        
    def stop(self):
        """Stop reporting weights.

        """
        self._sensor.pause()
        self._sensor.set_callback(None)
