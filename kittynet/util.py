"""Utility pipelines for Kittynet.

This package defines a number of message-processing pipelines for
Kittynet. None of these pipelines depend on I/O, or on the specific
protocols used by any client or server, but are rather machines to
convert streams of data coming in from clients into a useful form for
servers. Most of these pipelines operate nonthreaded by default,
because they in principle shouldn't ever block continued
operation. User-defined variants of these pipelines, however, might
need to be threaded, if they are likely to cause delays in execution.

"""


from .common import Pipeline,Client
from numbers import Number
import threading
import datetime
import time
import copy
import numpy
import scipy.stats

class Watchdog(Pipeline):
    """Watches servers which have start() and stop() methods for
    failure. If they cease to send messages for some configurable
    period of time, the watchdog will restart them, and pass on an
    alert message to its clients.

    To function properly, the watchdog must be attached to and
    detached from cleints with its watch() and unwatch() methods, and
    not with the ordinary Server.add_client(), Client.add_server(), or
    common.chain() methods.
    """
    def __init__(self,name="Watchdog",time=600):
        self.name=name
        if(isinstance(time,datetime.timedelta)):
            self.time=time.total_seconds()
        else:
            self.time=time
        self.time=time
        self._watchlist={}
        super().__init__(threaded=False)

    def watch(self,server,time=None):
        if time is None:
            time=self.time
        elif(isinstance(time,datetime.timedelta)):
            time=time.total_seconds()
        self._watchlist[server.name]={"server":server,
                                      "time":time,
                                      "alarm":None}
        server.add_client(self)

    def unwatch(self,server):
        if self._watchlist[server.name]["alarm"] is not None:
            self._watchlist[server.name]["alarm"].cancel()
        del self._watchlist[server.name]
        server.del_client(self)

    def _alarm(self,server,name):
        self.broadcast_data({"name":self.name,
                             "server":name})
        server.stop()
        server.start()

    def handle_data(self,message):
        """Starts an alert timer for the given server.

        Parameters
        ----------
        message : dict
            Message received from a server.

        """
        # We _shouldn't_ be a client to any server we're not
        # watching, but just in case....
        if (message["name"] in self._watchlist):
            if self._watchlist[message["name"]]["alarm"] is not None:
                self._watchlist[message["name"]]["alarm"].cancel()
                
            self._watchlist[message["name"]]["alarm"]=threading.Timer(
                self._watchlist[message["name"]]["time"],
                self._alarm,
                args=[self._watchlist[message["name"]]["server"],
                          message["name"]])
            
            self._watchlist[message["name"]]["alarm"].start()
        
class Transformer(Pipeline):

    """Transforms messages passing through with a given function. Does not
    deepcopy automatically; transform function must do that.

    Parameters
    ----------
    transform_function : function
        Function which takes a dict (representing a message) as its
        sole argument, and returns a dict represeting the desired
        transformation of the message. Note that modifying a dict may
        have undesirable effects on its usage elsewhere, so it is
        strongly recommend that the transformation function start with
        a :meth:`copy.deepcopy`.

    threaded : bool, optional
        Whether to give the transformer its own processing thread. Disabled
        by default.

    """
    def __init__(self,transform_function,threaded=False):
        self._transform=transform_function
        super().__init__(threaded=threaded)

    def handle_data(self,message):
        """Broadcasts a transformed version of the message received.

        Parameters
        ----------
        message : dict
            Message received from a server to be passed to clients.

        """

        self.broadcast_data(self._transform(message))

class Filter(Pipeline):
    """Filters messages passing through based on some criterion. Does not
    deepcopy but merely passes messages on.

    Parameters
    ----------
    filter_function : function
        Function which takes a dict (representing a message) as its
        sole argument, and returns a boolean indicating whether or not
        to pass that message along.

    threaded : bool, optional
        Whether to give the filter its own processing thread. Disabled
        by default.

    """
    def __init__(self,filter_function,threaded=False):
        self._filter_function=filter_function
        super().__init__(threaded=threaded)

    def handle_data(self,message):
        """Broadcasts the message received only if it satisfies the filter
        function.

        Parameters
        ----------
        message : dict
            Message received from a server to be passed to clients.

        """
        if self._filter_function(message):
            self.broadcast_data(message)

class BatteryAlert(Filter):
    """Passes a message once when each device's battery level goes below a
    certain alert threshold. Does not pass another message for that
    device until the battery is above a reset threshold.
    """
    def __init__(self,alert=20,reset=80):
        self.alert=alert
        self.reset=reset
        self.live={}
        super().__init__(self._filter_function)
        
    def _filter_function(self,message):
        if ("addr" not in message) or ("battery" not in message):
            return False  # Not a battery-information message
        if message["battery"]==-1:
            return False  # Special case: -1 is "no battery report"
        if message["addr"] not in self.live:
            # First emssage from this address; enable alert
            self.live[message["addr"]]=True
        if message["battery"]>self.reset:
            # Above reset threshold; re-enable alert
            self.live[message["addr"]]=True
        elif (self.live[message["addr"]] and message["battery"]<self.alert):
            # Below alert threshold for first time; report and disable
            self.live[message["addr"]]=False
            return True
        return False

class Deactivator(Filter):
    """Disables passthrough temporarily when a specific message source is
    received. Trigger messages are not passed through.

    Parameters
    ----------
    triggername : str

        The name of the server (as provided in the message's
        ``"name"`` field) which will trigger deactivation.

    sleeptime : float, int, or :class:`datetime.timedelta`, optional
        how long to disable passthrough (in seconds if numeric) after
        receiving a trigger message. One minute by default.

    toggle_mode : bool or None, optional
        If `True`, then a trigger message during the sleep period will
        turn passthrough back on early. If `False`, then a trigger
        message during the sleep period will extend the sleep period,
        resetting the timer. If `None` (the default behavior), trigger
        messages during the sleep period are ignored.

    Attributes
    ----------
    sleeptime : :class:`datetime.timedelta`
        how long to disable passthrough (in seconds if numeric) after
        receiving a trigger message.

    toggle_mode : bool or None, optional
        If `True`, then a trigger message during the sleep period will
        turn passthrough back on early. If `False`, then a trigger
        message during the sleep period will extend the sleep period,
        resetting the timer. If `None`, trigger messages during the
        sleep period are ignored.
    """
    def __init__(self,triggername,sleeptime=60,toggle_mode=None):
        self.triggername=triggername
        self._triggertime=None
        self.toggle_mode=toggle_mode
        if(isinstance(sleeptime,Number)):
            self.sleeptime=datetime.timedelta(seconds=sleeptime)
        else:
            self.sleeptime=sleeptime
        super().__init__(self._filter_function)

    def _filter_function(self,message):
        if (self._triggertime is not None):
            # Trigger is active
            if (message["time"]-self._triggertime>self.sleeptime):
                # Trigger has expired, turn passthrough back on
                self._triggertime=None
            else:
                # Unexpired trigger, act only on trigger messages
                if (self.toggle_mode is None or
                    message["name"]!=self.triggername):
                    return False
                elif (self.toggle_mode):
                    # Toggle trigger
                    self._triggertime=None
                    return False
                else:
                    # Extend trigger
                    self._triggertime=message["time"]
                    return False
        # If it drops through to here, passthrough is active
        if (message["name"]==self.triggername):
            self._triggertime=message["time"]
            return False
        else:
            return True


class Aggregator(Pipeline):
    """Agggregates messages classified by their ``"name"`` field, using an
    average (or other function) to merge their values, and posts an
    aggregated statistic when certain criteria are met.

    Automatically adds (overwriting, if already present) a field with
    key ``"msg_count"`` to the message sent, containing the number of
    messages aggregated.

    Parameters
    ----------
    entries : int, optional
        Number of messages after which all messages in the buffer are
        automatically aggregated. If zero (the default), then the
        count of messages in the buffer will not trigger automatic
        aggregation.

    interval : float, int, or :class:`datetime.timedelta`, optional
        how large an interval of time (in seconds if numeric) between
        the first and last message will trigger an aggregation. If
        ``None`` (the default), breadth of time in the buffer will not
        trigger automatic aggregation.

    time_last : bool or None, optional
        The aggregated message can be timestamped with the time the
        first message came in by setting this parameter ``False``, the
        time the last message came in by setting this parameter
        ``True`` (default behavior), or by their average if it is set
        ``None``.

    numeric_aggregator : function, optional
        The function to be used to aggregate numeric data in messages;
        any numeric field present in any key will use this function to
        produce an aggregate value for the message ultimately sent on
        aggregation. This function should take a list of numbers as
        input and return a number. By default this function is
        :meth:`numpy.mean`, so that numeric data will be aggregated by
        taking its arithmetic average.

    keyed_aggregators : dict of function, optional
        A dict of aggregator functions to be used for specific
        keys. This will override numeric_aggregator for any key
        present in this dict. The keys should be equal to message
        keys, and each value should be a function which takes a list
        of a single type as input and returns something. By default
        this dict is empty so that no key-specific aggregators are
        used.

    default_aggregator : function, optional
        The function to be used to aggregate non-numeric values in
        messages which do not possess a key-specific aggregator;
        this is the fallback for any key other than ``"time"`` or
        ``"name"``, both of which are used by the aggregator
        itself. This function should take a list of a single type as
        input and returns something. By default this function is set
        to return its parameter, so that non-numeric data is
        aggregated into a list.

    windowed : bool, optional

        When False, it gathers data points, reports when the
        history is full, and then purges the history (default
        behavior). When True, it keeps a "sliding window" of the last
        several data points, and reports every time a new data point
        comes in.

    threaded : bool, optional
        Whether to give the aggregator its own processing thread. Disabled
        by default.

    Attributes
    ----------
    entries : int
        Number of messages after which all messages in the buffer are
        automatically aggregated.

    interval : :class:`datetime.timedelta`
        How large an interval of time between the first and last
        message will trigger an aggregation.

    time_last : bool or None
        Which timestamp to use on aggregated messages.

    numeric_aggregator : function
        The function to be used to aggregate numeric data in messages.

    keyed_aggregators : dict of function
        A dict of aggregator functions to be used for specific
        keys.

    default_aggregator : function
        The function to be used to aggregate non-numeric values in
        messages which do not possess a key-specific aggregator.

    """
    def __init__(self,entries=0,interval=None,
                 time_last=True,
                 numeric_aggregator=numpy.mean,
                 keyed_aggregators={},
                 default_aggregator=lambda a:a,
                 windowed=False,
                 threaded=False):
        self.buffers={}
        self.entries=entries
        if(isinstance(interval,Number)):
            self.interval=datetime.timedelta(seconds=interval)
        else:
            self.interval=interval
        self.time_last=time_last
        self.numeric_aggregator=numeric_aggregator
        self.keyed_aggregators=keyed_aggregators
        self.default_aggregator=default_aggregator
        self._windowed=windowed
        super().__init__(threaded=threaded)
        
    def flush(self):
        """Flushes the buffers of every name which has been received. This 
        may broadcast several messages.

        Note that this function will be called when the aggregator is
        deleted (for instance, by program termination), so that
        partial reports get sent.

        Does nothing to a windowed buffer.
        """
        if not self._windowed:
            for name in self.buffers:
                flush_name(name)
            
    def flush_name(self,name):
        """Flushes the buffer of all messages received with the given "name"
        field. Will broadcast a message, unless there are no messages
        associated with this name.

        If the aggregator is windowed, this flushes only the stale
        data points, not the new ones.
        """

        if not self.buffers[name]:
            # Empty buffers; don't pass on a report
            return 
        if self._windowed:
            # Purge old entries
            if (self.entries > 0):
                while len(self.buffers[name]) >= self.entries:
                    self.buffers[name].pop(0)
            if self.interval is not None:
                while (self.buffers[name][-1]["time"]-self.buffers[name][0]["time"] >= self.interval):
                    self.buffers[name].pop(0)                    
        slicedDict={}
        for msg in self.buffers[name]:
            for key in msg:
                if(key=="name" or key=="time"):
                    pass
                elif key in slicedDict:
                    slicedDict[key].append(msg[key])
                else:
                    slicedDict[key]=[msg[key]]
        if self.time_last:
            message={"name":name,"time":self.buffers[name][-1]["time"]}
        elif self.time_last==False:
            message={"name":name,"time":self.buffers[name][0]["time"]}
        else:
            # A bit of a hack to "average" two datetime objects.
            message={"name":name,"time":self.buffers[name][0]["time"]+(self.buffers[name][-1]["time"]-self.buffers[name][0]["time"])/2}
        for key in slicedDict:
            if key in self.keyed_aggregators:
                message[key]=self.keyed_aggregators[key](slicedDict[key])
            elif all(isinstance(x,Number) for x in slicedDict[key]):
                message[key]=self.numeric_aggregator(slicedDict[key])
            else:
                message[key]=self.default_aggregator(slicedDict[key])
        message["msg_count"]=len(self.buffers[name])
        self.broadcast_data(message)
        if not self._windowed:
            self.buffers[name]=[]

    def handle_data(self,message):
        """Checks to see if the buffer associated with the given name is
        "full", according to the criteria determined by
        :attr:`entries` or :attr:`interval`. If so, flushes the buffer
        before ading the newest message to it.

        """
        if(message["name"] in self.buffers):
            if(self._windowed or
               (self.entries > 0 and
                len(self.buffers[message["name"]]) >= self.entries) or
               (self.interval is not None and
                message["time"] - self.buffers[message["name"]][0]["time"] >= self.interval)):
                self.flush_name(message["name"])
        else:
            self.buffers[message["name"]]=[]
        self.buffers[message["name"]].append(copy.deepcopy(message))

class WeightProcessor(Pipeline):
    """Takes in weight messages, and passes along reports of "heavy"
    objects (cats) and residual leavings after the heavy objects
    (excreta). Extended periods of stable weight result in normalizing
    to a new "tare" weight.

    This processor has no intrinsic units. The specific units on the
    weights it processes are determined by whatever weight-supplying
    server is sending it data. For that reason parameters which are
    weights do not have defaults --- reasonable defaults would be
    wildly different for measurements in pounds vs. ounces
    vs. kilograms vs. grams.

    Parameters
    ----------
    weight_threshold: float or int
        The weight above which a sustained measurement is interpreted
        as being "notable" (i.e., probably a cat in the box); below
        this weight is regarded as an empty or near-empty box (litter,
        excreta, potential taring errors).

    tare_variability: float or int
        The permissable variation in weight to be considered for
        "tare" purposes.

    transition_time: float, int, or :class:`datetime.timedelta`, optional
        How long anomolous readings can be regarded as "noise" rather
        than a state change. This affects transition from "low" to
        "high", as well as disrupting a tare stability
        measurement. The default of two seconds can deal with most
        HX711 glitches.

    leavings_time: float, int, or :class:`datetime.timedelta`, optional
        How long a "low" window (in seconds, if numeric) to take into
        consideration when calculating the weight of leavings; this
        should probably be less than tare_time. By defualt this is one
        minute.

    tare_time: float, int, or :class:`datetime.timedelta`, optional
        How long a reading must be "stable" (with stability defined by
        the tare_variability range) to insitgate a tare.

    """
    def __init__(self,weight_threshold,
                 tare_variability,
                 transition_time=2,
                 leavings_time=60,
                 tare_time=240):

        self._low_history=[]
        self._high_history=[]
        self._tare_history=[]
        self._is_low=True
        self._report_leavings=False
        self.tare_point=None
        self.weight_threshold=weight_threshold
        self.tare_variability=tare_variability
        if(isinstance(transition_time,Number)):
            self.transition_time=datetime.timedelta(seconds=transition_time)
        else:
            self.transition_time=transition_time
        if(isinstance(leavings_time,Number)):
            self.leavings_time=datetime.timedelta(seconds=leavings_time)
        else:
            self.leavings_time=leavings_time
        if(isinstance(tare_time,Number)):
            self.tare_time=datetime.timedelta(seconds=tare_time)
        else:
            self.tare_time=tare_time
        super().__init__(threaded=False)

    def _broadcast_leavings(self,message):
        self.broadcast_data({
            "name":message["name"],
            "time":self._low_history[0][0],
            "length":message["time"]-self._low_history[0][0],
            "weight":numpy.median([row[1] for row in self._low_history])-self.tare_point,
            "wtype":"leavings"})
        self._report_leavings=False
            
    def handle_data(self,message):
        if "weight" not in message:
            # Not a weight report; ignore
            return
        if self.tare_point is None:
            # Uncritically tare if there's no tare point
            self.tare_point=message["weight"]
            self.broadcast_data({
                "name":message["name"],
                "time":message["time"],
                "length":datetime.timedelta(seconds=0),
                "weight":message["weight"],
                "wtype":"tare"})
            return
        if (message["weight"]-self.tare_point)>self.weight_threshold:
            # Weight is high
            self._high_history.append([message["time"],message["weight"]])
            if self._is_low:
                # We're in a "low" phase; check to see if we've been
                # high for long enough to switch
                if (message["time"]-self._high_history[0][0])>self.transition_time:
                    # Switch to "high" phase.
                    self._is_low=False
                    if self._report_leavings:
                        # A weird, unlikely case; if we go from high
                        # to low and then back to high before
                        # reporting leavings, we need to report them then.
                        self._broadcast_leavings(message)

                    # We include a weight in this report just so that
                    # processors who depend on it won't choke and die,
                    # but as a practical matter this reported weight
                    # isn't very useful.
                    if(self._low_history):
                        self.broadcast_data({
                            "name":message["name"],
                            "time":message["time"],
                            "length":message["time"]-self._low_history[0][0],
                            "weight":message["weight"]-self.tare_point,
                            "wtype":"lowend"})
                        self._low_history=[]
                    else:
                        # This doesn't happen very often, and needs to
                        # be special-cased.
                        self.broadcast_data({
                            "name":message["name"],
                            "time":message["time"],
                            "length":datetime.timedelta(seconds=0),
                            "weight":message["weight"]-self.tare_point,
                            "wtype":"lowend"})
                        
            else:
                # We're in a "high" phase; purge the low history
                self._low_history=[]
                # Probably not as big a deal as the low history
                # filling up (see below), but it couldn't hurt to have
                # a purge for high history too.
                while len(self._high_history)>1 and message["time"]-self._high_history[1][0]>1.25*self.tare_time:
                    self._high_history.pop(1)
        else:
            # Weight is low
            self._low_history.append([message["time"],message["weight"]])
            if self._is_low:
                # We're in a "low" phase; purge the high history
                self._high_history=[]
                if self._report_leavings and (message["time"]-self._low_history[0][0])>self.leavings_time:
                    # We've been low for a little while; broadcast
                    # excreta weight.
                    self._broadcast_leavings(message)

                # An issue which could come up: extremely long times
                # in low phase would result in the _low_history array
                # becoming enormous. To prevent that, we'll go ahead
                # and old entries _except_ for the first (for timing
                # purposes).
                while len(self._low_history)>1 and message["time"]-self._low_history[1][0]>1.25*self.tare_time:
                    self._low_history.pop(1)                
            else:
                # We're in a "high" phase; check to see if we've been
                # low long enough to switch
                if (message["time"]-self._low_history[0][0])>self.transition_time:
                    # Switch to "low" phase.
                    self._is_low=True
                    # Broadcast the previous "high" weight
                    if self._high_history:
                        self.broadcast_data({
                            "name":message["name"],
                            "time":self._high_history[0][0],
                            "length":message["time"]-self._high_history[0][0],
                            "weight":numpy.median([row[1] for row in self._high_history])-self.tare_point,
                            "wtype":"high"})
                        self._report_leavings=True
                        self._high_history=[]
                    else:
                        # This shouldn't ever happen, but we
                        # special-case it anyways.
                        self.broadcast_data({
                            "name":message["name"],
                            "time":message["time"],
                            "length":datetime.timedelta(seconds=0),
                            "weight":message["weight"]-self.tare_point,
                            "wtype":"lowend"})
        # Regardless of state, perform tare maintenance
        self._tare_history.append([message["time"],message["weight"]])
        while (message["time"]-self._tare_history[0][0]>1.25*self.tare_time):
            # Purge obsolete records (i.e., anything old enough to be
            # surely irrelevant)
            self._tare_history.pop(0)
        tare_stability_start=message["time"]
        bounce_start=None
        tare_range_max=message["weight"]
        tare_range_min=message["weight"]
        for datapoint in reversed(self._tare_history):
            # Walk through the data backwards, widening the range of
            # points from the present
            if(datapoint[1]<tare_range_min and tare_range_max-datapoint[1]<=self.tare_variability):
                tare_range_min=datapoint[1]
            elif(datapoint[1]>tare_range_max and datapoint[1]-tare_range_min<=self.tare_variability):
                tare_range_max=datapoint[1]
            if(datapoint[1]>=tare_range_min and datapoint[1]<=tare_range_max):
                # In our stability range, so this is a "good" point
                bounce_start=None
                tare_stability_start=datapoint[0]
            else:
                if bounce_start is None:
                    bounce_start=datapoint[0]
                elif bounce_start-datapoint[0]>self.transition_time:
                    # Too much instability; break our walk
                    break

        # Now tare_stability_start should be the start of the longest
        # sustained period of being in the stability window. If it's
        # long enough, perform a tare.
        if (message["time"]-tare_stability_start>self.tare_time):
            self.broadcast_data({
                "name":message["name"],
                "time":message["time"],
                "length":message["time"]-tare_stability_start,
                "weight":message["weight"],
                "wtype":"tare"})
            self.tare_point=message["weight"]

class RSSIWeightPackager(Pipeline):
    """Packages minimum RSSI measurements during a weight event, based on
    reported weight-event messages. This class should be a client of
    both a WeightProcessor (to provide weight events) and at least one
    Bluetooth device (to provide RSSI reports).

    Parameters
    ----------
    backtrack : float, int, or :class:`datetime.timedelta`, optional
        how much of a history interval to keep to "backtrack" into
        when a weight-high event happens. Typically this should be at
        least as large as the transition_time parameter of the
        WeightProcessor feeding it data. If it's too low, not much bad
        happens, but the RSSI data is less accurate; if it's too high,
        also nothing bad will happen unless you're ridiculously low on
        memory, but you'll be keeping around unnecessary data. The
        default of 10 is considerably larger than the defautl
        transition_time and will usually be sufficient.


    Attributes
    ----------
    backtrack : :class:`datetime.timedelta`
        how much of a history interval to keep to "backtrack" into
        when a weight-high event happens.
    """
    def __init__(self,backtrack=2):
        if(isinstance(backtrack,Number)):
            self.backtrack=datetime.timedelta(seconds=backtrack)
        else:
            self.backtrack=backtrack
        # Named stages:
        # True --- high, recording rssis
        # False --- low; _may_ be a message in dispatch queue
        self._report_stage=False
        self._rssi_history=[]
        self._message=None
        super().__init__()

    def handle_data(self,message):
        if "rssi" in message:
            # An RSSI report; add it to history and purge irrelevant history
            self._rssi_history.append(copy.deepcopy(message))
            if self._report_stage is None:
                # Not recording; purge history older than the
                # "backtrack". We assume RSSI reports come in
                # chronologically, which usually is a reasonable
                # assumption.
                while(self._rssi_history[0]["time"]<message[time]-self.backtrack):
                    self._rssi_history.pop(0)
        if "wtype" in message:
            # A weight event; act on it as appropriate
            if message["wtype"]=="lowend":
                # Usually this occurs when we're currently in
                # _report_stage False, usually with no message
                # waiting. If there is a message waiting, it's
                # probably incomplete but we'll push it out. Only if
                # things have gone horribly wrong would we be in True,
                # and if so we're best ignoring it.
                if not self._report_stage:
                    if self._message is not None:
                        if not "leavings" in self._message:
                            self._message["leavings"]=0
                        self.broadcast_data(self._message)
                        self._message=None
                    # Purge records before lowend time, and start recording
                    while(self._rssi_history and self._rssi_history[0]["time"]<message["time"]-self.backtrack):
                        self._rssi_history.pop(0)
                    self._report_stage=True
            elif message["wtype"]=="high":
                # We expect to receive this when _report_stage is
                # True. If it's not, bad things are happening and
                # we're probably happier ignoring it.
                if self._report_stage:
                    # Start building a message, including "name",
                    # "time", "length", "weight", and "rssis" fields.
                    self._report_stage=False
                    self._message={"name":message["name"],
                                   "time":message["time"],
                                   "length":message["length"],
                                   "weight":message["weight"],
                                   "rssis":{}}
                    for rssimsg in self._rssi_history:
                        if rssimsg["time"]>=message["time"]:
                            if (rssimsg["name"] not in self._message["rssis"]
                                or rssimsg["rssi"]>self._message["rssis"][rssimsg["name"]]):
                                self._message["rssis"][rssimsg["name"]]=rssimsg["rssi"]
                    self._rssi_history=[]
            elif message["wtype"]=="leavings":
                # We should only get this when a message is in the
                # queue; if it's not, we'll ignore it, but if so,
                # we'll finish that message and fire it off.
                if self._message is not None:
                        self._message["leavings"]=message["weight"]
                        self.broadcast_data(self._message)
                        self._message=None
            elif message["wtype"]=="clean":
                # An alert of a clean cycle. Any currently ongoing message is
                # probably spurious, so we'll kill it and cease RSSI recording.
                self._message=None
                self._report_stage=False

class RSSITriggeredReporter(Client):

    """Records RSSI (as a measurement, broadly, of proximity) for each
    device, starting when a message satisfies the trigger function
    """
    def __init__(self,trigger,lowestRSSI=-100):
        self.triggertime=datetime.datetime.now()
        self.trigger=trigger
        self.records={}
        self.lowestRSSI=lowestRSSI
        super().__init__()
        
    def handle_data(self,message):
        if (self.trigger(message)):
            # Trigger function means to purge all records
            self.triggertime=datetime.datetime.now()

            self.records={}
        elif ("rssi" not in message) or ("name" not in message):
            # Not a Bluetooth message; ignore
            return
        elif message["name"] not in self.records:
            self.records[message["name"]]=message["rssi"]
        elif message["rssi"]>self.records[message["name"]]:
            self.records[message["name"]]=message["rssi"]

    def report(self,name):
        if name not in self.records:
            return self.lowestRSSI
        else:
            return self.records[name]

    def reportall(self):
        reportdict=copy.deepcopy(self.records)
        return {"rssi":reportdict,"triggerlength":(datetime.datetime.now()-self.triggertime).total_seconds()}

class RSSITimedReporter(Client):
    """Records RSSI (as a measurement, broadly, of proximity) for each
    device, maintaining records for a set interval of time; when asked
    to report, returns max RSSI (i.e. minimum proximity)
    """
    def __init__(self,interval=1860,lowestRSSI=-100):
        if(isinstance(interval,Number)):
            self.interval=datetime.timedelta(seconds=interval)
        else:
            self.interval=interval
        self.lowestRSSI=lowestRSSI
        self.records={}
        super().__init__()

    def purge_old_records(self,name,rssi):
        # Remove records which are either older than the interval, or
        # lower than (and presumably older than) the given, new RSSI.
        for timepoint in list(self.records[name]):
            if ((datetime.datetime.now()-timepoint)>self.interval) or (self.records[name][timepoint]<rssi):
                del self.records[name][timepoint]
        
    def handle_data(self,message):
        if ("rssi" not in message) or ("name" not in message):
            # Not a Bluetooth message; ignore
            return
        if message["name"] not in self.records:
            self.records[message["name"]]={}
        self.records[message["name"]][message["time"]]=message["rssi"]
        self.purge_old_records(message["name"],message["rssi"])

    def report(self,name):
        self.purge_old_records(name,self.lowestRSSI)
        maxRSSI=self.lowestRSSI
        for rssi in self.records[name].values():
            if rssi>maxRSSI:
                maxRSSI=rssi
        return maxRSSI

    def reportall(self):
        reportdict={}
        for name in self.records.keys():
            reportdict[name]=self.report(name)
        return {"rssi":reportdict,"triggerlength":self.interval.total_seconds()}
            
class RSSIReportAppender(Pipeline):
    """Appends RSSI reports to messages passing through. Takes a single
    RSSIReporter object on initialization, and requests a report on
    each passthrough, storing the report dict in the rssi key of the
    message."""
    def __init__(self,reporter,straythreshold=None):
        self.reporter=reporter
        if(isinstance(straythreshold,Number)):
            self.straythreshold=datetime.timedelta(seconds=straythreshold)
        else:
            self.straythreshold=straythreshold
        super().__init__()

    def handle_data(self,message):
        reports=self.reporter.reportall()
        if self.straythreshold and self.straythreshold.total_seconds()<reports["triggerlength"]:
            newmessage=copy.deepcopy(message)
            for key in reports:
                newmessage[key]=reports[key]
            self.broadcast_data(newmessage)

class CleanerControl(Pipeline):

    """Takes RSSI and weight-event input to provide output to turn on and
off a cleaning system, with the intent of running a cleaning cycle
after every usage but making sure _not_ to run when a cat is too close
to the machine.

    Parameters
    ----------
    runlength : float, int, or :class:`datetime.timedelta`
        Total time (in seconds, if numeric) to run the cleaning cycle
        after a usage event. The stacktimes and fullcycle parameters
        affect how this is translated into actual run lengths.

    delay : float, int, or :class:`datetime.timedelta`
        How long (in seconds, if numeric) to wait after a usage event
        (at minimum) before starting a cleaning cycle. This may be
        extended based on the RSSI limit.

    rssilimit : int
        How high the RSSI ID on a connected tracker should be to
        terminate (or not start) a cycle. If this is too low in a
        small or cat-ful house, then the clean cycle may _never_ run;
        too high, and you risk running a clean cycle with a cat
        nearby. -70 or thereabouts is safe for a medium-size house
        with internal walls; if the house is smaller or the litter box
        unusually centrally located you might need to kick that number
        downwards to -80 or less.

    rssifade : float, int, or :class:`datetime.timedelta`
        How long (in seconds, if numeric) to allow a high RSSI (see
        rssilimit parameter) to prevent cleaning. Default is one minute.

    stacktimes : bool
        If false (default), then a new weight event before the end of
        cleaning will cause the time left in the cleaning cycle to be
        set to runlength; if true, then the time left in the cleaning
        cycle will be _extended_ by runlength. This is probably a bad
        idea unless you are absolutely sure that weight events
        correspond to cat usage.

    fullcycle : bool
        If false (default), then it will continue attempting to run a
        clean cycle until the total cleaning time equals runtime; if
        true, then it keeps attempting runs until a _single_ clean
        cycle can run for the full time.

    Attributes
    ----------
    runlength : float
        Total time in seconds to run the cleaning cycle
        after a usage event. The stacktimes and fullcycle parameters
        affect how this is translated into actual run lengths.

    delay : float
        How long in seconds to wait after a usage event
        (at minimum) before starting a cleaning cycle. This may be
        extended based on the RSSI limit.

    rssilimit : int
        How high the RSSI ID on a connected tracker should be to
        terminate (or not start) a cycle.

    rssifade : float
        How long in seconds to allow a high RSSI (see
        rssilimit parameter) to prevent cleaning.

    stacktimes : bool
        If false, then a new weight event before the end of
        cleaning will cause the time left in the cleaning cycle to be
        set to runlength; if true, then the time left in the cleaning
        cycle will be _extended_ by runlength.

    fullcycle : bool
        If false, then it will continue attempting to run a
        clean cycle until the total cleaning time equals runtime; if
        true, then it keeps attempting runs until a _single_ clean
        cycle can run for the full time.

    """
    def __init__(self,runlength,delay,rssilimit,rssifade=60,stacktimes=False,fullcycle=False,name="Cleaner Control"):
        # Initially, it has no "time left", and is inactive
        self._timeleft=0
        # None means inactive; otherwise, epoch time is used.
        self._activated_at=None

        # Since messages are handled sequentially, we should never
        # have any sort of race condition on threads, but there are a
        # few timed actions too, and I can't say with absolute
        # certainty that they won't bump into each other. Safer just
        # to have a lock, really.
        self._lock=threading.Lock()
        self._runtimer=None
        self._delaytimer=None
        if(isinstance(runlength,datetime.timedelta)):
            self.runlength=runlength.total_seconds()
        else:
            self.runlength=runlength
        if(isinstance(delay,datetime.timedelta)):
            self.delay=delay.total_seconds()
        else:
            self.delay=delay
        if(isinstance(rssifade,datetime.timedelta)):
            self.rssifade=rssifade.total_seconds()
        else:
            self.rssifade=rssifade
        self.stacktimes=stacktimes
        self.fullcycle=fullcycle
        self.rssilimit=rssilimit
        self.name=name
        super().__init__()

    def handle_data(self,message):
        """Data received is either: a weight incident report, which is used to
        trigger a cleaning cycle, or an RSSI report, which is used
        (when high-value) to start a delay cycle.
        """
        if "wtype" in message and message["wtype"]=="high":
            # Weight event, start a delayed cycle.
            self._lock.acquire()
            if self.stacktimes:
                self.timeleft=self.timeleft+self.runlength
            else:
                self.timeleft=self.runlength
            self._lock.release()
            self.pause(self.delay)
        elif "rssi" in message and "rssi">=self.rssilimit:
            # RSSI event, force delay on cycle.
            self.pause(self.rssifade)

    def activate(self):
        """(Re)activates the cleaning cycle for the necessary amount of time;
        once done, it stops. If cycle is already active, then it does
        nothing. This will terminate the delay timer.
        """
        if self.activated_at is None and self._timeleft > 0:
            if self._delaytimer is not None:
                self._delaytimer.cancel()
                self._delaytimer=None
            self._runtimer=thread.Timer(self._timeleft,self.pause)
            self.broadcast_data({"name":self.name,
                                 "time":datetime.datetime.now(),
                                 "value":True})
            self.activated_at=time.time()
            self._runtimer.start()
        self._lock.release()
        
    def stop(self):
        """Stops the cleaning cycle, if active. If inactive, does
        nothing."""
        self._lock.acquire()
        if self.activated_at is None:
            self._lock.release()
            return
        if self._runtimer is not None:
            self._runtimer.cancel()
            self._runtimer=None
        self.broadcast_data({"name":self.name,
                             "time":datetime.datetime.now(),
                             "value":False})
        self.activated_at = None
        self._timeleft=self._timeleft - (time.time()-self.activated_at)
        # This should never happen, but just in case...
        if self.timeleft>0 and self.fullcycle:
            self._timeleft=self.runlength
        elif self._timeleft<0:
            self.timeleft=0

        self._lock.release()
        
    def pause(self,delay):
        """Pauses the cleaning cycle, if active, and starts a timer to
        (re)activate the cycle after the given number of seconds.
        """
        self.stop()
        self._lock.acquire()
        if self._delaytimer is not None:
            self._delaytimer.cancel()
            self._delaytimer=None
        self._delaytimer=thread.Timer(delay,self.activate)
        self._delaytimer.start()
        self._lock.release()

class CleanerPipeswitch(Pipeline):
    """When running a clean cycle, you usually want to disable throughput
    of data (especially weight data) during the cleaning cycle; this
    is a pipeline which will pass through non-cleaner-control data
    under ordinary circumstances, but while any cleaner control which
    is a server of this pipeline is active, it'll pass through no
    messages.

    Parameters
    ----------
    cleaner : None, str, or function
        The means to use to determine which incoming messages are
        "cleaner control messages". None (the default) assumes any
        message with a "value" field is a cleaner control message. A
        string matches only the cleaner messages with that particular
        "name" field. A function taking a message as input and
        returning a boolean will be used as a per-message query as to
        which messages are cleaner control messages.

    Attributes
    ----------
    cleaner : None, str, or function
        The means to use to determine which incoming messages are
        "cleaner control messages".
    """

    def __init__(self,cleaner=None):
        self.cleaner=cleaner
        self._passthru=True
        self._active_cleaners=set()
        super().__init__()


    def handle_data(self,message):
        """If it's a cleaner-control message, update the active cleaners list
        and don't pass it through; if it isn't, pass it through if
        there are no active cleaners.
        """
        if(self.cleaner is None):
            cc_test = ("value" in message)
        elif callable(self.cleaner):
            cc_test = self.cleaner(message)
        else:
            cc_test = (message["name"]==self.cleaner)
        if cc_test:
            if message["value"]:
                self._active_cleaners.add(message["name"])
            else:
                self._active_cleaners.remove(message["name"])
        else:
            if not self._active_cleaners:
                self.broadcast_data(message)
                
