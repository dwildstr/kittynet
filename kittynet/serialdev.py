"""Modules for communicating with serial devices.

This implements, at present, just a simple server packaging single
lines of text taken off of a serial ine into messages.
"""

import serial
import threading
import time
import datetime
import binascii
import re
import glob
import os

from .common import Server

class SerialReader(Server):
    """Serial monitor to read reports off of serial input.

    I use this to read wash-initiation reports from an Arduino
    monitoring a CatGenie.

    An Arduino monitoring a CatGenie 120 (used as an open-source
    variant of the CartridgeGenius DRM-cracking hardware) can, among
    other things, recognize when a cleaning cycle begins because the
    CatGenie polls the fake RFID reader which the Arduino is
    masquerading as. If that poll produces output on USB (or any other
    serial bus), it can be sent to a KittyNet monitoring station. This
    module has been tested with an Arduino connected over USB to a
    KittyNet Raspberry Pi Zero W.

    Parameters
    ----------
    device : str, optional

        Path of a serial device, or a wildcard glob to be matched to a
        serial device; by default it is ``"/dev/ttyACM*"``, which is
        the USB serial device under Raspbian on a Pi Zero W (usually
        /dev/ttyACM0 in particular, but in case of disconnect it may
        become /dev/ttyACM1 or /dev/ttyACM2. On different OSes or
        devices it may be something else. If a wildcard is used, then
        the first match from :meth:`glob.glob` will be selected; note
        that there is no established order for files returned by this
        method.

    baud : int, optional
        Baudrate of the serial port (9600 by default).

    name : str, optional
        Common human-readable name for the device, to be attached to
        all messages.

    timeout : float, optional
        Timeout (in seconds) for a line to be read. In principle,
        unless the serial channel is unusually slow or bursty, the
        default of 5 seconds should be more than enough.

    Attributes
    ----------
    device : str

        Path of a serial device, or a wildcard glob to be matched to a
        serial device.

    baud : int
        Baudrate of the serial port.

    name : str
        Common human-readable name for the device, to be attached to
        all messages.

    timeout : float
        Timeout (in seconds) for a line to be read.

    running : bool
        Indicates whether the serial-monitoring thread is active, and
        used to control its activity.

    """
    def __init__(self,device="/dev/ttyACM*",baud=9600,name="Serial device",timeout=5):
        self.name=name
        self.device=device
        self.baud=baud
        self.timeout=timeout
        self._serial=None
        self._thread=threading.Thread(target=self._scanloop)
        self.running=False
        super().__init__()

    def _reconnect(self):
        devices=False
        while self.running and not devices:
            devices=glob.glob(self.device)
            if not devices:
                    time.sleep(0.1)
        if devices:
            self._current_device=devices[0]
            self._serial=serial.Serial(port=self._current_device,baudrate=self.baud,timeout=self.timeout)
            self._serial.flush()
                
    def _scanloop(self):
        self._reconnect()
        while self.running:
            if (os.path.exists(self._current_device)):
                try:
                    if (self._serial.in_waiting > 0):
                        line = self._serial.readline().decode('utf-8').rstrip()
                        match=re.search(r"\d+",line)
                        if match:
                            rdata=match.group(0)
                        else:
                            rdata="0"
                        self.broadcast_data({"name":self.name,
                                             "time":datetime.datetime.now(),
                                             "data":int(rdata)})
                    time.sleep(0.1)
                except OSError:
                    self._reconnect()
            else:
                self._reconnect()
                
    def __del__(self):
        self.stop()

    def start(self):
        """Sets the serial port-listener thread running.

        If the thread is already running, this method does nothing.

        """
        self._thread=threading.Thread(target=self._scanloop)
        self.running=True
        self._thread.start()
#        self.broadcast_data({"name":self.name,
#                             "time":datetime.datetime.now(),
#                             "data":"Start"})

    def stop(self):
        """Terminates the serial port-listener thread, after the current line
        if a line is already being read.

        If the thread is not running, this method does nothing.

        """
        if self._serial:
            self._serial.close()
        self.running=False
        self._thread.join()
