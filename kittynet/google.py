"""Google services integration for KittyNet

This module provides clients which make use of the Google API to
record or report messages. Specifically, it provides the means to
email a form letter through GMail, or to record data in a Google
Sheet.

"""

import googleapiclient.errors
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from http.client import HTTPException


from email.mime.text import MIMEText

from .common import Client

import base64
import datetime
import pickle
import copy
import threading

SCOPES_GMAIL = ['https://www.googleapis.com/auth/gmail.send']
SCOPES_SHEETS = ['https://www.googleapis.com/auth/spreadsheets']
SCOPES_ALL = SCOPES_GMAIL + SCOPES_SHEETS


class ServiceManager(object):
    """A class to provide a single API pathway to a single Google
    service. Any number of MailAlerters with different intended
    subject lines/recipients/messages can use a single ServiceManager
    providing the GMail API, and any number of SheetsLogger recording
    to different spreadsheets can use a single ServiceManager
    providing the GMail API.

    MailAlerters will require a ServiceManager instantiated with
    ``service='gmail'`` and ``api=v1``.

    SheetsLoggers will require a ServiceManager instantiated with
    ``service='sheets'`` and ``api=v4``.

    Parameters
    ----------
    credentials : str
        Path of a credentials file to use; this should be a json file
        downloaded from the Google Cloud Platform. See various Google
        API "Quickstart" documents for assistance in retrieving this
        file.

    service : str
        Name of the service which this manager is providing. At
        present only ``'gmail'`` and ``'sheets'`` have clients
        implemented to make use of them, but the GServiceManager can
        serve any Google Cloud Services API in principle.

    api : str
        Version number of the API to be provided by the manager. At
        present only GMail API ``'v1'`` and Sheets API ``'v4'`` are
        implemented in the clients.

    scopes : list of str (optional)
        The list of OAuth2 scopes which the manager is reequesting
        from Google. By default this will request both the relevant
        GMail and Sheets scopes, but it can be limited to one or the
        other. In particular, the constants
        ``kittynet.gservices.SCOPES_GMAIL`` and
        ``kittynet.gservices.SCOPES_SHEETS`` provide GMail and Sheets
        access respectively.

    token : str (optional)
        The path of a token file to use. If you don't want to
        reauthenticate every time a manager is created, it is very
        useful to have a stored token. If this is a new file, it will
        be created after authentication.

    use_console : bool (optional)
        Whether to use the console for authentication (enabled by
        default), or to open a browser window. On an embedded device
        like a Raspberry Pi, console is necessary, although browser
        authentication is smoother and easier.

    Attributes
    ----------
    service : :class:`googleapiclient.discovery.Resource`
        The Resource used to interact with the API. Specific subclass
        may vary depending on what API is being interacted with.

    lock : :class:`threading.Lock`
        A lock used to ensure that at most one object is using this
        service-manager at a time.

    """
    def __init__(self,credentials,service,api,scopes=SCOPES_ALL,token=None,use_console=True):
        self._use_console=use_console
        self._token=None
        self._tokenfile=token
        self._credentialsfile=credentials
        self._scopes=scopes
        self.lock=threading.RLock()
        self._service=service
        self._api=api
        self.connect()
        super().__init__()        

    def connect(self):
        """Connect (or reconnect) to the service being provided. This is done
automatically on initialization but may need to be re-executed if the
connection is lost.

        """

        try:
            with open(self._tokenfile,'rb') as tokenhandle:
                self._token=pickle.load(tokenhandle)
        except:
            pass
        if self._token and self._token.refresh_token:
            self._token.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                self._credentialsfile, self._scopes)
            if self._use_console:
                self._token = flow.run_console()
            else:
                self._token = flow.run_local_server()
            # Save the credentials for the next run
            if self._tokenfile is not None:
                with open(self._tokenfile, 'wb') as tokenhandle:
                    pickle.dump(self._token, tokenhandle)
        self.service=build(self._service,self._api,credentials=self._token,cache_discovery=False)
        
class GMailAlerter(Client):
    """A client which sends received messages as emails through GMail. The
    exact format of the messages can be specified as a text string
    which is populated from the message's values.

    Parameters
    ----------
    manager : ServiceManager
        A ServiceManager which provides the GMail API. The manager
        must be instantiated with the ``service='gmail',api=='v1'``
        parameters.

    recipient : str
        Identity of the recipient. This string will be formatted using
        the method :meth:`str.format` and can include
        placeholders expanded into message values, if the recipient is
        to be determined by information in the message.

    sender : str
        Identity of the sender. This string will be formatted using
        the method :meth:`str.format` and can include
        placeholders expanded into message values, if the sender is to
        be determined by information in the message. Note that GMail
        may limit you to sending messages from an address associated
        with the credentials provided.

    subject : str
        Subject line of the messages sent. This string will be formatted using
        the method :meth:`str.format` and can include
        placeholders expanded into message values.

    text : str
        Text of the messages sent. This string will be formatted using
        the method :meth:`str.format` and can include
        placeholders expanded into message values.

    Attributes
    ----------
    recipient : str
        Identity of the recipient.

    sender : str
        Identity of the sender.

    subject : str
        Subject line of the messages sent.

    text : str
        Text of the messages sent.
    """

    def __init__(self,manager,recipient,sender,subject="{subject}",text="{msg}"):
        self._manager=manager;
        self.recipient=recipient
        self.sender=sender
        self.subject=subject
        self.text=text
        super().__init__()        

    def handle_data(self,message):
        """The message being handled is converted into a GMail message and
        posted using the Google API.

        Parameters
        ----------
        message : dict
            Message received from a server, to be used to populate
            fields in an email.

        """
        
        messageObj=MIMEText(self.text.format(**message))
        messageObj['to'] = self.recipient
        messageObj['from'] = self.sender        
        messageObj['subject'] = self.subject.format(**message)
        message = {'raw': base64.urlsafe_b64encode(messageObj.as_string().encode('utf_8')).decode('utf-8')}
        response=None
        with self._manager.lock:
            while response is None:
                try:
                    response=self._manager.service.users().messages().send(userId="me",body=message).execute()
                except (HTTPException,ConnectionResetError,googleapiclient.errors.HttpError):
                    self._manager.connect()

class SheetsLogger(Client):
    """A client which apends received messages to a Google Sheet. What
    content is placed in each row is determined by the message's
    values and can be customized.

    Parameters
    ----------
    manager : ServiceManager
        A ServiceManager which provides the Sheets API. The manager
        must be instantiated with the ``service='sheets',api=='v4'``
        parameters.

    sheet_id : str or None
        The sheet_id (in Google Sheets) of an extant document to edit.

    sheet_name : str, optional
        If `sheet_id` is ``None``, then a new spreadsheet is created,
        with the user-readable name given. Note that even if a sheet
        with this name already exists, a new one will be created!

    pagename : str, optional
        Page on which data will be written. This string will be
        formatted using the method :meth:`str.format` and can include
        placeholders expanded into message values. By default it is
        'Sheet1', a title common to newly created spreadsheets.

    rangesuffix : str, optional
        A suffix to the range to be appended. This is rarely useful
        unless you want to constrain your data to a specific range on
        a spreadsheet.

    rowspec : list of str
        A list of text with which to populate the cells. The strings
        will be formatted using the method :meth:`str.format` and can
        include placeholders expanded into message values. By default
        it will put just the time (formatted for Google Sheets).

    Raises
    ------
    ValueError
        If neither `sheet_id` nor `sheet_name` is specified.

    Attributes
    ----------
    recipient : str
        Identity of the recipient.

    sender : str
        Identity of the sender.

    subject : str
        Subject line of the messages sent.

    text : str
        Text of the messages sent.

    """
    def __init__(self,manager,sheet_id=None,sheet_name=None,rowspec=['{time}'],pagename='Sheet1',rangesuffix=''):
        self._manager=manager
        self.rowspec=rowspec
        self.pagename=pagename
        self.rangesuffix=rangesuffix
        if (sheet_id is not None):
            self.sheet_id=sheet_id
        elif (sheet_name is not None):
            spreadsheet_creator = {
                'properties': {
                    'title': sheet_name
                }
            }
            self._manager.lock.acquire()
            spreadsheet = self._manager.service.spreadsheets().create(body=spreadsheet_creator,fields='spreadsheetId').execute()
            self.sheet_id=spreadsheet.get('spreadsheetId')
            self._manager.lock.release()
        else:
            raise ValueError("Sheet must be identified by a sheet_id or sheet_name")
        super().__init__()        

    def handle_data(self,message):
        """The message being handled is used to populate a list, which is
        appended as a row to a sheet using the Google API.

        Parameters
        ----------
        message : dict
            Message received from a server, to be used to populate
            lists representing cells in a row of a spreadsheet.

        """

        worksheet=None
        with self._manager.lock:
            while worksheet is None:
                try:
                    worksheet = self._manager.service.spreadsheets().get(spreadsheetId=self.sheet_id).execute()
                except (HTTPException,ConnectionResetError,googleapiclient.errors.HttpError) as e:
                    self._manager.connect()
            pagenames=[]
            pages = worksheet.get('sheets', [])
            for page in pages:
                pagenames=[page.get("properties",{}).get("title", "Sheet1")
                           for page in pages]
            if self.pagename.format(**message) not in pagenames:
                # Create new page!
                returnval = None
                while returnval is None:
                    try:
                        returnval=self._manager.service.spreadsheets().batchUpdate(
                            spreadsheetId=self.sheet_id,
                            body={'requests':  [
                                {
                                    'addSheet':{
                                        'properties':{'title': self.pagename.format(**message)}
                                }}
                            ]}).execute()
                    except (HTTPException,ConnectionResetError,googleapiclient.errors.HttpError) as e:
                        self._manager.connect()
            # There is basically never a reason to put a date into Google
            # Sheets in a different form, so we've built in a strftime
            # here.
            messageFmt=copy.deepcopy(message)
            messageFmt["time"]=message["time"].strftime("%Y %b %d %H:%M:%S")        
            row=list(map(lambda x: x.format(**messageFmt),self.rowspec))
            returnval = None
            while returnval is None:
                try:
                    returnval=self._manager.service.spreadsheets().values().append(
                        spreadsheetId=self.sheet_id,
                        range=self.pagename.format(**message) + self.rangesuffix,
                        valueInputOption='USER_ENTERED',
                        body={'values':  [row]}).execute()
                except (HTTPException,ConnectionResetError,googleapiclient.errors.HttpError) as e:
                    self._manager.connect()
