"""Common elements of the client-server messaging framework of KittyNet.

Messages are dicts, which contain several individual parts of a
report. The only mandated key for a message is ``"time"``, which
always contains a timestamp of the time to which the report
dates. Other keys in messages currently implemented in kittynet
include:

  * ``"name"``: human-readable name of a reporting entity
  * ``"addr"``: address (MAC, IP, etc.) of a reporting device
  * ``"rssi"``: signal strength on a Bluetooth message
  * ``"battery"``: battery percentage report from a device
  * ``"weight"``: weight report from a load sensor

Servers which measure or report other statistics may wish to extend
this standard; any keys can be used in a message.

Clients each implement their owh thread to handle messages, so that a
posted message will not delay further operation of the server.

"""

import threading
import queue

class Server(object):
    """Base class for any server of data; produces no data by default.

    Objects in this class are specifically servers of messages. The
    purpose of a server is to broadcast messages to several different
    handlers of those messages, called clients. A general-purpose
    server of this specific class will broadcast data only on the
    invocation of the :meth:`broadcast_data` method; specific-purpose
    servers may broadcast data automatically based on internal state.

    Attributes
    ----------
    clients : list of :class:`~Client`
        individual clients which receive served data

    """

    def __init__(self):
        self.clients=[]
        super().__init__()

    def add_client(self,client):
        """Adds the given parameter to the list of clients to which this server
        broadcasts.

        Parameters
        ----------
        client : Client
            Client to be added to the broadcast list.

        Raises
        ------
        TypeError
            If `client` is not a recognized kittynet :class:`Client`.

        """
        if not isinstance(client, Client):
            raise TypeError('add_client reqires a kittynet.common.Client object')
        self.clients.append(client)

    def del_client(self,client):
        """Removes the given parameter from the list of clients to which this
        server broadcasts.

        This routine does not check to see if the client in question
        is actually in the broadcast list, or even if it's a client.

        Parameters
        ----------
        client : Client
            Client to be removed from the broadcast list.

        """
        self.clients.remove(client)

    def broadcast_data(self,message):
        """Sends a particular message to all of this server's
        clients. Extensions of the Server class, particularly
        :class:`~Pipeline` and its subclasses, may call this
        automatically as they process data.

        Parameters
        ----------
        message : dict
            Message to be broadcast to clients.
        """

        for client in self.clients:
            client.post_data(message)

class Client(object):
    """Base class for a receiver of data; ignores data by default.

    Objects in this class are specifically clients of "messages":
    dicts which contain individual discrete data points. The purpose
    of a client is to handle messages sent to it by emitters of such
    messages, called servers. A general-purpose client of this
    specific class simply ignores messages received; specific-purpose
    clients shoudl have additional functionality added specifically to
    the :meth:`handle_data` method.

    Clients may have their own associated threads, or not. If a client
    does not have an associated thread, then it will block further
    execution whenever :meth:`post_data` is called. If a client does
    something which takes a significant amount of time with the data
    (e.g. wireless or network communication to either amend it or log
    it), then this may bog down actions being taken
    elsewhere. Generally speaking, it is advisable to thread all I/O
    clients. Lightweight clients may be unthreaded, although unless
    performance problems emerge then there's probably not a reason any
    clients need to be unthreaded.

    Parameters
    ----------
    threaded : bool (optional)
        Whether to give the client its own processing thread. Enabled
        by default.

    Attributes
    ----------
    threaded : bool
        Whether to give the client its own processing thread.
    """

    def __init__(self,threaded=True):
        self.threaded=threaded
        if threaded:
            self._threadlock=threading.Lock()
            self._thread=threading.Thread()
            self._message_queue=queue.Queue()
        super().__init__()

    def add_server(self,server):
        """Registers this client as receiving messages from the given parameter.

        Parameters
        ----------
        server : Server
            Server with which this client is to be registered

        Raises
        ------
        TypeError
            If `server` is not a recognized kittynet :class:`Server`.

        """
        if not isinstance(server, Server):
            raise TypeError('add_server reqires a kittynet.common.Server object')
        server.add_client(self)

    def del_server(self,server):
        """Deregisters this client as receiving messages from the given
        parameter.

        If this client is already not receiving messages from the
        given parameter, this method does nothing.

        Parameters
        ----------
        server : Server
            Server with which this client is to be registered

        Raises
        ------
        TypeError
            If `server` is not a recognized kittynet :class:`Server`.

        """
        if not isinstance(server, Server):
            raise TypeError('add_server reqires a kittynet.common.Server object')
        server.del_client(self)

    def __del():
        self.flush_messages()
        self.flush()
        
    def flush_messages(self):
        """Instructs the client to flush any messages.

        Messages ideally should not be able to build up in a way which
        requires intervention, but it may be good practice to call
        this method whenever no more messages are expected, e.g. when
        the program is terminating, or this client is being deleted or
        deregistered, or its servers are being deleted or disabled.
        """
        while not self._message_queue.empty():
            self.handle_data(self._message_queue.get())
        
    def flush(self):
        """Instructs the client to flush any internally-accumulated data.

        This method is invoked whenever a client should generate any
        reports of partial information which it would ordinarily
        produce from some "completed" collection of received
        messages. Examples of the circumstances under which it might
        need to be invoked include when the server is disabled or
        deregistered (so that no more messages would be expected), or when
        the client itself is being deleted, or when some entity
        receiving reports from the clients is due to be closed.
        
        In the base class, no data is accumulated internally and so
        flushing does nothing. Subclasses which do have internal state
        should add functionality to this method.

        """
        pass

    def handle_data(self,message):
        """Code for the client to process a message sent to it.

        Useful clients will act on the message being received,
        processing it in some way.

        In the base class, no specific processing is mandated and so
        this method does nothing. Subclasses add functionality to this
        method.

        Please note that the same message may be sent to multiple
        asynchronously processing clients, and so it is very
        inadvisable to modify the message in this method, as doing so
        may change what is being seen by other clients. If
        modification of the message is useful or necessary, it's
        advisable to deepcopy it first.

        Parameters
        ----------
        message : dict
            Message received (hopefully recently) from a server.

        """
    def post_data(self,message):
        """Sends a message to the client.

        This method is invoked automatically when a server connected
        to the client broadcasts a message. It processes
        (asynchronously) the message received as well as any other
        messages which may have built up.

        Actual handling is relegated to the :meth:`handle_data` method.

        Parameters
        ----------
        message : dict
            Message received from a server.

        """
        if self.threaded:
            self._message_queue.put(message)
            self._threadlock.acquire()
            if not self._thread.is_alive():
                self._thread=threading.Thread(target=self.flush_messages)
                self._thread.start()
            self._threadlock.release()
        else:
            self.handle_data(message)

class Pipeline(Client,Server):
    """Base class for a pipeline; passes data through by default.

    Pipelines are objects which serve both as clients and servers in
    the Kittynet message-passing system, implicity serving
    specifically as a unit which acts to process incoming data and
    then pass it along.

    The basic pipeline simply passes all messages directly from its
    servers to its clients. Pipelines which perform more extensive
    processing will override the :meth:`handle_data` and possibly
    :meth:`flush` methods.

    """

    def handle_data(self,message):
        """The basic pipeline simply broadcasts whatever data it receives.

        Parameters
        ----------
        message : dict
            Message received from a server to be passed to clients.
        """
        self.broadcast_data(message)

def chain(*args):
    """Builds a message "chain" from a server through any number of
    pipelines to a client. A quick shorthand for several add_client
    calls, and also one that lets you anonymously refer to newly
    instantiated pipelines or clients that will only be used in this
    one message flow.

    """
    current_server=None
    for x in args:
        if(current_server is not None):
            current_server.add_client(x)
        current_server=x
