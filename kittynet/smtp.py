"""SMTP client for Kittynet

This module provides a client which connects over SMTP to send
messages as emails. SSL is used to connect because doing it over plaintext is a terrible idea.
"""

import smtplib, ssl

from .common import Client

import threading

class Mailer(Client):
    """A client which sends received messages as emails over SMTP. The
    exact format of the messages can be specified as a text string
    which is populated from the message's values.

    Parameters
    ----------
    server : str
        Hostname for the SMTP server.

    username : str
        Login name to use on the SMTP server.

    password : str
        Password to use on the SMTP server.

    sender : str
        Identity of the sender. This string will be formatted using
        the method :meth:`str.format` and can include placeholders
        expanded into message values, if the sender is to be
        determined by information in the message. Note that your SMTP
        server may limit you to sending messages from an address
        associated with your login.

    recipient : str
        Identity of the recipient. This string will be formatted using
        the method :meth:`str.format` and can include
        placeholders expanded into message values, if the recipient is
        to be determined by information in the message.

    text : str
        Text of the messages sent. This includes any headers, so that
        e.g. "Subject:" lines are in this. This string will be
        formatted using the method :meth:`str.format` and can include
        placeholders expanded into message values.

    port : int, optional
        Port to use to connect to the SMTP server. By default this is
        587, which is the correct starttls port for all but unusual setups.

    context : ssl.SSLContext, optional
        Used to set up special security policies; by default the
        system's standard context is used, which is sufficient for
        most purposes.

    Attributes
    ----------
    server : str
        Hostname for the SMTP server.

    username : str
        Login name to use on the SMTP server.

    password : str
        Password to use on the SMTP server.

    sender : str
        Identity of the sender.

    recipient : str
        Identity of the recipient.

    text : str
        Text of the messages sent.

    port : int
        Port to use to connect to the SMTP server.
    """

    def __init__(self,server,username,password,sender,recipient,text="Subject: {subject}\n\n{msg}",port=587,context=None):
        
        if context is None:
            self._ssl_context=ssl.create_default_context()
        else:
            self._ssl_context=context
        self.server=server
        self.username=username
        self.password=password
        self.port=port
        self.sender=sender
        self.recipient=recipient
        self.text=text
        super().__init__()        

    def handle_data(self,message):
        """The message being handled is converted into a GMail message and
        posted using the Google API.

        Parameters
        ----------
        message : dict
            Message received from a server, to be used to populate
            fields in an email.
        """

        server=smtplib.SMTP(self.server, self.port)
        server.starttls(context=self._ssl_context)
        server.login(self.username,self.password)
        server.sendmail(self.sender,self.recipient, self.text.format(**message))
        server.quit()            
