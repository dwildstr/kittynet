"""General-purpose GPIO device support for KittyNet.

The only input devices supported at present are simple digital one-pin
triggers, which go high during an event and low when the event is
over. Outputs include a number of variant on LEDs which encode data in different ways.
"""

from .common import Server,Client,Pipeline
from .util import Filter
from numbers import Number

from gpiozero import DigitalInputDevice, LED, PWMLED, RGBLED, Button
import datetime
import time
import threading


class SimpleTrigger(Server):
    """Simple trigger interface: reads edge from given GPIO pin. Good for,
    e.g. a PIR sensor, or any other one-bit detection device.

    On activation of the device (if the trigger is set up to report
    activation), this object broadcasts a message with the ``"value"``
    field set to 1; on deactivation (if the trigger is set up to
    report deactivation), this object broadcasts a message with the
    ``"value"`` field set to 0.

    Parameters
    ----------
    pin : int or str
        GPIO pin number (using BCM numbering) to be monitored; see
        documentation on :class:`gpiozero.InputDevice` for full usage.
    pull_up : bool or None
        Which mode to use for internal pull-up/pull-down of the pin;
        see documentation on :class:`gpiozero.InputDevice` for full
        usage.
    active_state : bool or None
        Whether to associate the activation state to the pin voltage
        or to its reverse; see documentation on
        :class:`gpiozero.InputDevice` for full usage.
    name : str
        common human-readable name for the device
    activate : bool
        whether the trigger sends a report on activation (rising edge)
    deactivate : bool
        whether the trigger sends a report on deactivation (falling edge)

    Attributes
    ----------
    activate : bool
        whether the trigger sends a report on activation (rising edge)
    deactivate : bool
        whether the trigger sends a report on deactivation (falling edge)
    name : string
        common human-readable name for the device

    """
    def __init__(self,pin,pull_up=None,active_state=True,name="Simple Trigger",activate=True,deactivate=False):
        self.activate=activate
        self.deactivate=deactivate
        self.name=name
        self._gpiozero=DigitalInputDevice(pin=pin,pull_up=pull_up,active_state=active_state)
        self.start()
        super().__init__()

    def _edge_detect(self):
        self.broadcast_data({"name":self.name,
                             "time":datetime.datetime.now(),
                             "value":self.gpiozero.value})

    def __del__(self):
        self.stop()

    def stop(self):
        """Disables all reporting and GPIO monitoring.

        """
        self._gpiozero.when_activated = None
        self._gpiozero.when_deactivated = None
        
    def start(self):
        """Enable reporting of whichever are applicable of activation and
        deactivation states.

        """

        if self.activate:
            self._gpiozero.when_activated = self._edge_detect
        if self.deactivate:
            self._gpiozero.when_deactivated = self._edge_detect

class SimpleControl(Client):
    """Simple digital control interface: sets given GPIO pin high or low
    as insructed. Good for, e.g. a relay-controlled system (I have an
    SSR running vibration motors for self-cleaning of a pine-pellet
    sifting litterpann; this is but one example of what you could do
    with this).

    On receipt of messages with a "value" field, this sets the GPIO
    pin depending on that value, using standard Python interpretation
    of the value in a boolean context. Messages without a "value"
    field are ignored.

    Parameters
    ----------
    pin : int or str
        GPIO pin number (using BCM numbering) to be monitored; see
        documentation on :class:`gpiozero.DigitalOutputDevice` for full usage.
    active_high : bool Whether to associate the activation state to
        the pin voltage (default) or to its reverse; see documentation
        on :class:`gpiozero.DigitalOutputDevice` for full usage.

    """
    def __init__(self,pin,active_high=True,):
        self._gpiozero=DigitalOutputDevice(pin=pin,active_high=active_high)
        super().__init__(threaded=True)

    def handle_data(self,message):
        if("value" in message):
            if(message["value"]):
                self._gpiozero.on()
            else:
                self._gpiozero.off()

class LEDToggle(Client):
    """A simple client which flips an LED between on and off every time a
    message comes in. Good for keeping an easy visual monitor on a
    message-generating server.

    Parameters
    ----------
    pin : int or str
        GPIO pin number (using BCM numbering) for the LED; see
        documentation on :class:`gpiozero.LED` for full usage.

    slowdown : int, optional
        Factor to slow down toggle rate by, e.g. if this is set to 3,
        then the ligt toggles every three messages, instead of every
        message. By default this is 1. Values less than 1 should not
        be used.

    """
    def __init__(self,pin,slowdown=1):
        self._led=LED(pin)
        self.slowdown=slowdown
        self._countdown=slowdown
        super().__init__()

    def __del__(self):
        self._led.off()
        
    def handle_data(self,message):
        self._countdown -= 1
        if self._countdown<=0:
            self._countdown=self.slowdown
            self._led.toggle()

class LEDNumeric(Client):
    """A client which keeps track of the numeric `"data"` field in the
    most recent message sent, and blinks it out in a morse-code-like
    binary sequence of long pulses for "1" and short pulses for "0".

    Parameters
    ----------
    pin : int or str
        GPIO pin number (using BCM numbering) for the LED; see
        documentation on :class:`gpiozero.LED` for full usage.

    one_length : int, float, or :class:`datetime.timedelta`
        The length of a "1" signal in seconds. Default is 1.5 if
        zero_length is absent, and thrice zero_length if present.

    zero_length : int, float, or :class:`datetime.timedelta`
        The length of a "0" signal in seconds. Default is 0.5 if
        one_length is absent, and a third of one_length if present.

    window_length : int, float, or :class:`datetime.timedelta`
        The length of the window padding a "0" or "1" signal in
        seconds; this should exceed both one_length and zero_length,
        of course. The default is the maximum of those two, plus 0.5 seconds.

    pause_length : int, float, or :class:`datetime.timedelta`
        The length of the pause at the end of a signal repetition, in
        seconds. The default is thrice the smaller of one_length or
        zero_length.

    bits : int, optional
        If positive, use exactly this number of bits, zero-padding
        values too small and truncating values too large. If negative,
        use the absolute value of bits as the number of bits,
        zero-padding values too small but extending the message for
        values too long. If zero (default behavior), use a number of
        bits dictated by the value (and 1 bit to transmit the value
        0).

    big_endian: bool
        If True, blinks in binary with the least significant byte
        first; if False (default), blinks with the most significant
        byte first.

    """
    def __init__(self,pin,one_length=None,zero_length=None,window_length=None,pause_length=None,big_endian=False):
        self._led=LED(pin)
        self._blinker_thread=threading.Thread(target=self._blinkloop)

        if(isinstance(one_length,datetime.timedelta)):
            self.one_length=one_length.total_seconds()
        else:
            self.one_length=one_length
        if(isinstance(zero_length,datetime.timedelta)):
            self.zero_length=zero_length.total_seconds()
        else:
            self.zero_length=zero_length
        if self.one_length is None and self.zero_length is None:
            self.one_length=1.5
            self.zero_length=0.5
        elif self.one_length is None:
            self.one_length=self.zero_length*3
        elif self.zero_length is None:
            self.zero_length=float(self.one_length)/3

        if window_length is None:
            self.window_length=max(self.zero_length,self.one_length)+0.5
        elif(isinstance(window_length,datetime.timedelta)):
            self.window_length=window_length.total_seconds()
        else:
            self.window_length=window_length

        if pause_length is None:
            self.pause_length=min(self.zero_length,self.one_length)*3
        elif(isinstance(pause_length,datetime.timedelta)):
            self.pause_length=pause_length.total_seconds()
        else:
            self.pause_length=pause_length

        self.bits=bits
        self.big_endian=False
        self._data=0
        self.running=False
        super().__init__()

    def __del__(self):
        self.stop()
        self._led.off()
        
    def _blinkloop(self):
        while self.running:
            data_str=bin(self._data)[2:]
            if self.bits>0:
                data_str=(data_str.zfill(self.bits))[-self.bits:]
            elif self.bits<0:
                data_str=data_str.zfill(-self.bits)
            if self.big_endian:
                data_str=''.join(reversed(data_str))
            for bit in data_str:
                self._led.on()
                time.sleep(self.zero_length if bit=='0' else self.one_length)
                self._led.off()
                time.sleep(self.window_length-
                           (self.zero_length if bit=='0' else self.one_length))
            time.sleep(self.pause_length)

    def start(self):
        if self.running:
            return
        self._blinker_thread=threading.Thread(target=self._blinkloop)
        self.running=True
        self._blinker_thread.start()

    def stop(self):
        self.running=False
        self._blinker_thread.join()

    def handle_data(self,message):
        if "data" in message:
            self._data=message["data"]

class PWMLEDControl(Client):
    """A client which sets its value depending on the content of a
    message, using a supplied value function.

    Parameters
    ----------
    pin : int or str
        GPIO pin number (using BCM numbering) for the LED; see
        documentation on :class:`gpiozero.PWMLED` for full usage.

    frequency : int, optional
        frequency (in Hertz) to use for the PWM duty cycle. See
        documentation on :class:`gpiozero.PWMLED` for full usage.

    value_func : function
        Function which takes a dict, representing a message, and
        returns either a float in the range 0.0 to 1.0 or None. This
        function computes the intensity to set the PWM duty cycle at,
        or alternatively returns None to indicate no change to the LED.
    """
    def __init__(self,pin,value_func,frequency=100):
        self._led=PWMLED(pin=pin,frequency=frequency)
        self._value_func=value_func
        super().__init__()

    def __del__(self):
        self._led.off()
        
    def handle_data(self,message):
        newval=self._value_func(message)
        if newval is not None:
            self._led.value=newval

class PWMLEDPulse(PWMLEDControl):
    """A client which pulses at a rate determined by the rate at which
    messages arrive.

    Parameters
    ----------
    pin : int or str
        GPIO pin number (using BCM numbering) to be monitored; see
        documentation on :class:`gpiozero.PWMLED` for full usage.

    steps : float, optional
        Number of steps up or down; each message moves it one step;
        higher step_size will result in faster, more jagged pulses.

    frequency : int, optional
        frequency (in Hertz) to use for the PWM duty cycle. See
        documentation on :class:`gpiozero.PWMLED` for full usage.

    """
    def __init__(self,pin,steps=20,frequency=100):
        self.steps=steps
        self._direction=1
        self._value=0
        super().__init__(pin,value_func=self._value_func,frequency=frequency)

    def __del__(self):
        self._led.off()

    def _value_func(self,message):
        self._value += self._direction
        self._led.value=self._value/self.steps
        if(self._value==self.steps):
            self._direction=-1
        elif(self._value==0):
            self._direction=1

class RGBLEDControl(Client):
    """A client which controls a full-color LED depending on the content
    of a message, using a supplied value function. This assumes a
    common-cathode three-color LED.

    Parameters
    ----------
    red : int or str
        GPIO pin number (using BCM numbering) of the red pin; see
        documentation on :class:`gpiozero.RGBLED` for full usage.

    green : int or str
        GPIO pin number (using BCM numbering) of the green pin; see
        documentation on :class:`gpiozero.RGBLED` for full usage.

    blue : int or str
        GPIO pin number (using BCM numbering) of the blue pin; see
        documentation on :class:`gpiozero.RGBLED` for full usage.

    value_func : function
        Function which takes a dict, representing a message, and
        returns either a triple of floats in the range 0.0 to 1.0 or
        None. This function computes the color/intensity of the LED;
        None indicates no change.

    """
    def __init__(self,red,blue,green,value_func):
        self._led=RGBLED(red=red,blue=blue,green=green)
        self._value_func=value_func
        super().__init__()

    def __del__(self):
        self._led.off()
        
    def handle_data(self,message):
        newval=self._value_func(message)
        if newval is not None:
            self._led.value=newval


class RGBLEDCycle(RGBLEDControl):
    """A client which cycles through the color wheel at a rate determined
    by the rate at which messages arrive.

    Parameters
    ----------
    red : int or str
        GPIO pin number (using BCM numbering) of the red pin; see
        documentation on :class:`gpiozero.RGBLED` for full usage.

    green : int or str
        GPIO pin number (using BCM numbering) of the green pin; see
        documentation on :class:`gpiozero.RGBLED` for full usage.

    blue : int or str
        GPIO pin number (using BCM numbering) of the blue pin; see
        documentation on :class:`gpiozero.RGBLED` for full usage.

    steps : float, optional
        Number of steps on each leg of the triangle; each
        message moves it one step; lower steps value will result in
        faster, more jagged pulses.

    direction : bool
        If True (default), cycles from red to purple to blue to cyan
        to green to yellow; if False, cycles in the opposite
        direction.
    """
    def __init__(self,red,green,blue,steps=20,direction=True):
        self.steps=steps
        self.direction=1
        self._value=[steps,0,0]
        super().__init__(red=red,green=green,blue=blue,value_func=self._value_func)

    def __del__(self):
        self._led.off()

    def _value_func(self,message):
        if(self._value[0]>0 and self._value[2]==0):
            self._value[0]-=1
            self._value[1]+=1
        elif(self._value[1]>0 and self._value[0]==0):
            self._value[1]-=1
            self._value[2]+=1
        elif(self._value[2]>0 and self._value[1]==0):
            self._value[2]-=1
            self._value[0]+=1
        for i in range(3):
            if(self._value[i]>self.steps):
                self._value[i]=self.steps
        if self.direction:
            self._led.value=tuple([i/self.steps for i in self._value])
        else:
            self._led.value=tuple([i/self.steps for i in self._value].reverse())


class RGBLEDBTSource(RGBLEDControl):

    """A client which selects a color based on the strongest of the recent
    Bluetooth source messages sent to it.

    Parameters
    ----------
    red : int or str
        GPIO pin number (using BCM numbering) of the red pin; see
        documentation on :class:`gpiozero.RGBLED` for full usage.

    green : int or str
        GPIO pin number (using BCM numbering) of the green pin; see
        documentation on :class:`gpiozero.RGBLED` for full usage.

    blue : int or str
        GPIO pin number (using BCM numbering) of the blue pin; see
        documentation on :class:`gpiozero.RGBLED` for full usage.

    sources : dict
        Each entry in the list is a dict mapping name keys to color
        triples.

    decay : int, float, or :class:`datetime.timedelta`, optional
        Time (in seconds if numeric) after which a previous RSSI
        record "decays" and is no longer relevant to choosing the
        primary color (4 second default).

    blink_intensity : float, optional
        If a new report indicates an unchanged source, step down (or
        up, if appropriate) to the given multiple of that source's
        color. This will cause the LED to "blink" when there is one
        source consistently nearest. Setting this to 1 causes no
        blinking; 0 causes on/off blinking, and numbers between
        represent dimming. Values greater than 1 are only appropriate
        if all colors in use are less than full intensity. Default is
        0.5.

    """
    def __init__(self,red,green,blue,sources,decay=4,blink_intensity=0.5):
        self._sources=sources
        if(isinstance(decay,Number)):
            self._decay=datetime.timedelta(seconds=decay)
        else:
            self._decay=decay
        self._blink_intensity=blink_intensity
        self._last_rssi=-100
        self._last_source=""
        self._repeat=False
        self._last_time=datetime.datetime.now()
        super().__init__(red=red,green=green,blue=blue,value_func=self._value_func)

    def __del__(self):
        self._led.off()

    def _value_func(self,message):
        if message["name"]==self._last_source:
            if message["rssi"]>=self._last_rssi:
                self._last_rssi=message["rssi"]
                self._last_time=message["time"]
            if not self._repeat:
                self._repeat=True
                return tuple(self._blink_intensity*x for x in self._sources[message["name"]])
            else:
                self._repeat=False
                return self._sources[message["name"]]
        else:
            if (message["rssi"]>=self._last_rssi) or (message["time"]>self._last_time+self._decay):
                self._repeat=False
                self._last_rssi=message["rssi"]
                self._last_time=message["time"]
                self._last_source=message["name"]
                return self._sources[message["name"]]

class ToggleFilter(Pipeline):
    """A simple filter, which passes messages only when the switch (or
    button) is activated. Can work with either momentary or persistent
    switches.

    Parameters
    ----------
    pin : int or str
        GPIO pin number (using BCM numbering) to be monitored; see
        documentation on :class:`gpiozero.Button` for full usage.
    pull_up : bool or None, optional
        Which mode to use for internal pull-up/pull-down of the pin;
        see documentation on :class:`gpiozero.Button` for full
        usage.
    active_state : bool or None, optional
        Whether to associate the activation state to the pin voltage
        or to its reverse; see documentation on
        :class:`gpiozero.InputDevice` for full usage.
    bounce_time : float or None, optional
        Length to ignore changes in state afte an initial state
        change; see documentation on :class:`gpiozero.InputDevice` for
        full usage.
    momentary : bool, optional
        If False (the default), then data will be passed when the
        switch is on, and not passed when it is off; this is good for
        flipswitches and buttons which click into an "on" state. If
        True, then activation of the switch (putting it into the
        "active" state) will toggle whether data is passed through;
        this setting is good for momentary pushbuttons.
    """
    def __init__(self,pin,pull_up=True,active_state=None,bounce_time=None,momentary=False):
        self._button=Button(pin=pin,pull_up=pull_up,active_state=active_state,bounce_time=bounce_time)
        if momentary:
            self._value=True
            self._button.when_pressed=self._momentary_toggle
            filter_function=self._filter_function_momentary
        else:
            filter_function=self._filter_function_nonmomentary
        super().__init__()

    def _filter_function_momentary(self,message):
        """Passes messages when the switch (virtual, if momentary) is on.
        """
        return self._value

    def _filter_function_nonmomentary(self,message):
        return (self._button.value==1)

    def _momentary_toggle(self):
        self._value=not self._value
