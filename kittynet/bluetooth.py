"""Data-providers accessed over Bluetooth/BT-LE.

At present, this module only supports retrieving battery statistics
from Mi Band 2 hardware, and detecting RSSIs (which serve as a
reasonably proxy for distance) on all Bluetooth Low-Energy devices.

"""


from bluepy import btle
import datetime,threading,copy,struct,sys
from .common import Pipeline
from numbers import Number

# We require this so that no more than one thread tries to use the
# host controller interface at once.
_hci_lock=threading.RLock()

_MAX_RETRIES=2


class LEMonitor(object):
    """Monitor object for one or more BTLE devices.
    
    A class built around a thread which regularly scans Bluetooth LE
    devices; this thread then posts messages to individual device
    handlers, which are responsible for then connecting to those
    devices.

    Parameters
    ----------
    frequency : float, int, or :class:`datetime.timedelta`, optional
        how frequently (in seconds if numeric) the scanner polls

    Attributes
    ----------
    frequency : int, float, or :class:`datetime.timedelta`
        how frequently (in seconds) the scanner polls
    devices : list of :class:`~LEDevice`
        individual devices which will be searched for
    running : bool
        determiner of whether the polling thread continues to loop

    """

    def __init__(self,frequency=10.0):
        if(isinstance(frequency,datetime.timedelta)):
            self.frequency=frequency.total_seconds()
        else:
            self.frequency=frequency
        self._scanner=btle.Scanner()
        self.devices=[]
        self._thread=threading.Thread(target=self._scanloop)
        self.running=False
        super().__init__()
  
    def add_device(self,device):
        """Adds the given parameter to the list of devices being scanned.

        Parameters
        ----------
        device : LEDevice
            LEDevice to be added to the scan list.

        Raises
        ------
        TypeError
            If `device` is not a recognized bluetooth :class:`~LEDevice`.

        """
        if not isinstance(device, LEDevice):
            raise TypeError('add_device reqires a kittynet.bluetooth.LEDevice object')
        self.devices.append(device)

    def del_device(self,device):
        """Removes the given parameter to the list of devices being scanned.

        This routine does not check to see if the device in question
        is actually in the scan list, or even if it's a device.

        Parameters
        ----------
        device : LEDevice
            LEDevice to be removed from the scan list.
        """
        self.devices.remove(device)

    def start(self):
        """Sets the scanning thread running.

        If the thread is already running, this method does nothing.

        """
        if self.running:
            return
        self._thread=threading.Thread(target=self._scanloop)
        self.running=True
        self._thread.start()

    def stop(self):
        """Terminates the running thread after completing the current scan.

        If the thread is not running, this method does nothing.

        """
        self.running=False
        self._thread.join()
      
    def _scanloop(self):
        """Utility function to perform the scanning procedure and post
        messages to detected devices.

        There is unlikely to be a good reason to call, or even
        reference, this function directly. Using the start() and
        stop() methods to control the execution of this function, in a
        thread of its own, is best practices.

        """

        while self.running:
            devices=None
            while devices is None:
                with _hci_lock:
                    try:
                        devices = self._scanner.scan(self.frequency)
                        for dev in devices:
                            for client in self.devices:
                                if client.addr==dev.addr:
                                    client.post_data({
                                        "addr":dev.addr,
                                        "rssi":dev.rssi,
                                        "time":datetime.datetime.now()
                                    })
                    except (btle.BTLEDisconnectError, btle.BTLEManagementError):
                        # Reconnect a dead scanner and keep going
                        self._scanner=btle.Scanner()

class LEDevice(Pipeline):
    """A bare-bones representation of a Bluetooth Low Energy device.

    An instance of this specific class will simply attach a common name to
    messages and pass them otherwise unmodified. Communication with
    specific bluetooth devices, following their individual protocols,
    should be implemented in subclasses.

    Parameters
    ----------
    addr : string
        Standard MAC address presented in string form as six bytes
        separated by colons.
    addr_type : string
        Either the ``bluepy.btle.ADDR_TYPE_PUBLIC`` or
        ``bluepy.btle.RANDOM`` constant to denote the type of
        address possessed by the device.
    name : string
        User-readable name possessed by the device, used to
        populate the ``name`` field of messages sent by it.
    rssi_adjust : int
        Amount to automatically adjust message RSSIs by; this is used
        to normalize devices with unusually high or low signal
        strengths. Default is zero.

    Attributes
    ----------
    addr : string
        Standard MAC address presented in string form as six bytes
        separated by colons.
    addr_type : string
        Either the ``bluepy.btle.ADDR_TYPE_PUBLIC`` or
        ``bluepy.btle.RANDOM`` constant to denote the type of address
        possessed by the device.
    name : string
        User-readable name possessed by the device, used to populate
        the ``name`` field of messages sent by it.
    rssi_adjust : int
        Amount to automatically adjust message RSSIs by; this is used
        to normalize devices with unusually high or low signal
        strengths.
    """

    def __init__(self,addr,addr_type,name="Generic Bluetooth Device",rssi_adjust=0):
        self.name=name
        self.addr=addr
        self.addr_type=addr_type
        self.rssi_adjust=rssi_adjust
        super().__init__()
        
    def handle_data(self,message):
        """A simple generic device will only attach its name to messages
        passing through.

        Parameters
        ----------
        message : dict
            Message received from a server to be annotated and passed on.
        
        """
        newmessage=copy.deepcopy(message)
        newmessage["name"]=self.name
        newmessage["rssi"]+=self.rssi_adjust
        self.broadcast_data(newmessage)
            
class GATTDevice(LEDevice):
    """A Bluetooth LE device which provides the standard GATT Battery
    Service. This is very much a work in progress and hasn't really
    been made to work at all.

    This class, when it receives a message, will scan the associated
    device to update battery stats using the Bluetooth SIG-defined
    standards for requesting battery level.

    Parameters
    ----------
    addr : string
        Standard MAC address presented in string form as six bytes
        separated by colons.
    addr_type : string
        Either the ``bluepy.btle.ADDR_TYPE_PUBLIC`` or
        ``bluepy.btle.RANDOM`` constant to denote the type of
        address possessed by the device.
    name : string
        User-readable name possessed by the device, used to
        populate the ``name`` field of messages sent by it.
    rssi_adjust : int
        Amount to automatically adjust message RSSIs by; this is used
        to normalize devices with unusually high or low signal
        strngths. Default is zero.
    get_stats : bool
        Whether to test the battery at all, or behave like a "dumb
        device".
    update_interval : int, float, or :class:`datetime.timedelta`
        How frequently (in seconds if numeric) to update battery
        statistics.

    Attributes
    ----------
    addr : string
        Standard MAC address presented in string form as six bytes
        separated by colons.
    addr_type : string
        Either the ``bluepy.btle.ADDR_TYPE_PUBLIC`` or
        ``bluepy.btle.RANDOM`` constant to denote the type of address
        possessed by the device.
    name : string
        User-readable name possessed by the device, used to populate
        the ``name`` field of messages sent by it.
    rssi_adjust : int
        Amount to automatically adjust message RSSIs by; this is used
        to normalize devices with unusually high or low signal
        strengths.
    get_stats : bool
        Whether to test the battery at all, or behave like a "dumb device".
    update_interval : :class:`datetime.timedelta`
        How frequently to update battery statistics (hourly by default).
    last_update_time : :class:`datetime.datetime`
        Time of last update.
    last_battery_value : int
        Battery value at last update (-1 represents no battery information).

    """
    def __init__(self,addr,addr_type=btle.ADDR_TYPE_RANDOM,
                 name="Mi Band 2",rssi_adjust=0,get_stats=True,update_interval=3600):
        if(isinstance(update_interval,Number)):
            self.update_interval=datetime.timedelta(seconds=update_interval)
        else:
            self.update_interval=update_interval
        self.get_stats=get_stats
        self.last_update_time=datetime.datetime.now()
        self.last_battery_value=-1
        super().__init__(addr,addr_type,name,rssi_adjust)        

    def update_battery_data(self):
        """Communicate with Band in order to update battery statistics. If the
        update fails, it will silently not update the statistics.

        """
        
        with _hci_lock:
            retries=0
            while retries<_MAX_RETRIES: 
                try:
                    retries=retries+1
                    peripheral=btle.Peripheral(self.addr,self.addr_type)
                    # This is the "battery stats" characteristic.
                    ch=peripheral.getCharacteristics(uuid="00000006-0000-3512-2118-0009af100700")[0]
                    self.last_battery_data=ch.read()
                    self.last_battery_update_time=datetime.datetime.now()
                    self._parse_battery_data()
                    retries=_MAX_RETRIES+1
                except Exception as e:
                    # Yeah, "except pass" is terrible, but here all it'll
                    # do is simply not update battery stats on a failure.
                    pass

    def _parse_battery_data(self):
        """Convert the structured battery data into its constituent parts."""
        self.last_battery_value=struct.unpack('B',self.last_battery_data[1:2])[0]
        self.last_battery_data={}
        self.last_battery_data["charging"]=(struct.unpack('B',self.last_battery_data[2:3])[0]==1)
        self.last_battery_data["cycles"]=struct.unpack('B',self.last_battery_data[18:19])[0]
        self.last_battery_data["last_charge"]=datetime.datetime(
            struct.unpack('h',self.last_battery_data[11:13])[0],
            struct.unpack('B',self.last_battery_data[13:14])[0],
            struct.unpack('B',self.last_battery_data[14:15])[0],
            struct.unpack('B',self.last_battery_data[15:16])[0],
            struct.unpack('B',self.last_battery_data[16:17])[0],
            struct.unpack('B',self.last_battery_data[17:18])[0])
        self.last_battery_data["last_off"]=datetime.datetime(
            struct.unpack('h',self.last_battery_data[3:5])[0],
            struct.unpack('B',self.last_battery_data[5:6])[0],
            struct.unpack('B',self.last_battery_data[6:7])[0],
            struct.unpack('B',self.last_battery_data[7:8])[0],
            struct.unpack('B',self.last_battery_data[8:9])[0],
            struct.unpack('B',self.last_battery_data[9:10])[0])

    def handle_data(self,message):
        """When a Mi Band 2 is handling data, it takes the opportunity to
        update battery statistics. The resulting message is then
        annotated with the device's name and current battery report.

        Parameters
        ----------
        message : dict
            Message received from a server to be annotated and passed on.

        """

        if self.get_stats:
            if(self.last_battery_value<0 or (datetime.datetime.now()-self.last_battery_update_time) > self.update_interval):
                self.update_battery_data()
        newmessage=copy.deepcopy(message)
        newmessage["name"]=self.name
        newmessage["rssi"]+=self.rssi_adjust
        newmessage["battery"]=self.last_battery_value
        newmessage["batt_data"]=copy.deepcopy(self.last_battery_data)
        self.broadcast_data(newmessage)

class MiBand2(LEDevice):
    """An object to communicate with a Mi Band 2 activity tracker.

    This same class should also work with a Mi Band 3, 4, or 5, at
    least for battery statistics.

    At present this class, when it receives a message, will scan the
    associated device to update battery stats. Future iterations of
    this class may implement other data retrieval. At that time, code
    for the Mi Band 3, 4, or 5 may require a separate class.

    Parameters
    ----------
    addr : string
        Standard MAC address presented in string form as six bytes
        separated by colons.
    addr_type : string
        Either the ``bluepy.btle.ADDR_TYPE_PUBLIC`` or
        ``bluepy.btle.RANDOM`` constant to denote the type of
        address possessed by the device.
    name : string
        User-readable name possessed by the device, used to
        populate the ``name`` field of messages sent by it.
    rssi_adjust : int
        Amount to automatically adjust message RSSIs by; this is used
        to normalize devices with unusually high or low signal
        strngths. Default is zero.
    get_stats : bool
        Whether to test the battery at all, or behave like a "dumb
        device".
    update_interval : int, float, or :class:`datetime.timedelta`
        How frequently (in seconds if numeric) to update battery
        statistics.

    Attributes
    ----------
    addr : string
        Standard MAC address presented in string form as six bytes
        separated by colons.
    addr_type : string
        Either the ``bluepy.btle.ADDR_TYPE_PUBLIC`` or
        ``bluepy.btle.RANDOM`` constant to denote the type of address
        possessed by the device.
    name : string
        User-readable name possessed by the device, used to populate
        the ``name`` field of messages sent by it.
    rssi_adjust : int
        Amount to automatically adjust message RSSIs by; this is used
        to normalize devices with unusually high or low signal
        strengths.
    get_stats : bool
        Whether to test the battery at all, or behave like a "dumb device".
    update_interval : :class:`datetime.timedelta`
        How frequently to update battery statistics (hourly by default).
    last_update_time : :class:`datetime.datetime`
        Time of last update.
    last_battery_value : int
        Battery value at last update (-1 represents no battery information).

    """
    def __init__(self,addr,addr_type=btle.ADDR_TYPE_RANDOM,
                 name="Mi Band 2",rssi_adjust=0,get_stats=True,update_interval=3600):
        if(isinstance(update_interval,Number)):
            self.update_interval=datetime.timedelta(seconds=update_interval)
        else:
            self.update_interval=update_interval
        self.get_stats=get_stats
        self.last_update_time=datetime.datetime.now()
        self.last_battery_value=-1
        self.last_battery_data=None
        super().__init__(addr,addr_type,name,rssi_adjust)        

    def update_battery_data(self):
        """Communicate with Band in order to update battery statistics. If the
        update fails, it will silently not update the statistics.

        """
        
        with _hci_lock:
            retries=0
            while retries<_MAX_RETRIES: 
               try:
                    retries=retries+1
                    peripheral=btle.Peripheral(self.addr,self.addr_type)
                    # This is the "battery stats" characteristic.
                    ch=peripheral.getCharacteristics(uuid="00000006-0000-3512-2118-0009af100700")[0]
                    self.last_battery_data=ch.read()
                    self.last_battery_update_time=datetime.datetime.now()
                    self._parse_battery_data()
                    retries=_MAX_RETRIES+1
               except Exception as e:
                    # Yeah, "except pass" is terrible, but here all it'll
                    # do is simply not update battery stats on a failure.
    #                print(str(e))
                    pass

    def _parse_battery_data(self):
        """Convert the structured battery data into its constituent parts."""
        self.last_battery_value=struct.unpack('B',self.last_battery_data[1:2])[0]
        self.last_battery_data={
            "charging":struct.unpack('B',self.last_battery_data[2:3])[0]==1,
            "cycles":struct.unpack('B',self.last_battery_data[18:19])[0],
            "last_charge":datetime.datetime(
                struct.unpack('h',self.last_battery_data[11:13])[0],
                struct.unpack('B',self.last_battery_data[13:14])[0],
                struct.unpack('B',self.last_battery_data[14:15])[0],
                struct.unpack('B',self.last_battery_data[15:16])[0],
                struct.unpack('B',self.last_battery_data[16:17])[0],
                struct.unpack('B',self.last_battery_data[17:18])[0]),
            "last_off":datetime.datetime(
                struct.unpack('h',self.last_battery_data[3:5])[0],
                struct.unpack('B',self.last_battery_data[5:6])[0],
                struct.unpack('B',self.last_battery_data[6:7])[0],
                struct.unpack('B',self.last_battery_data[7:8])[0],
                struct.unpack('B',self.last_battery_data[8:9])[0],
                struct.unpack('B',self.last_battery_data[9:10])[0])
        }

    def handle_data(self,message):
        """When a Mi Band 2 is handling data, it takes the opportunity to
        update battery statistics. The resulting message is then
        annotated with the device's name and current battery report.

        Parameters
        ----------
        message : dict
            Message received from a server to be annotated and passed on.

        """

        if self.get_stats:
            if(self.last_battery_value<0 or (datetime.datetime.now()-self.last_battery_update_time) > self.update_interval):
                self.update_battery_data()
        newmessage=copy.deepcopy(message)
        newmessage["name"]=self.name
        newmessage["rssi"]+=self.rssi_adjust
        newmessage["battery"]=self.last_battery_value
        if self.last_battery_data is not None:
            newmessage["batt_data"]=copy.deepcopy(self.last_battery_data)
        self.broadcast_data(newmessage)
